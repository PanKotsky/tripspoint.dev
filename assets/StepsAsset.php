<?php

namespace app\assets;

use yii\web\AssetBundle;

class StepsAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery.steps';
    public $css = [
    ];
    public $js = [
        'build/jquery.steps.js',
    ];
}