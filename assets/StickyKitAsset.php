<?php

namespace app\assets;

use yii\web\AssetBundle;

class StickyKitAsset extends AssetBundle
{
    public $sourcePath = '@bower/sticky-kit';
    public $css = [
    ];
    public $js = [
        'jquery.sticky-kit.js',
    ];
}