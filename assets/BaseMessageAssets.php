<?php
/**
 * Created by PhpStorm.
 * User: VisioN
 * Date: 03.09.2015
 * Time: 10:28
 */

namespace vision\messages\assets;

use app\assets\AppAsset;
use \yii\web\AssetBundle;

class BaseMessageAssets extends AssetBundle {

    public $sourcePath = '@web';

}