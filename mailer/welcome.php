<?php

use yii\helpers\Html;

/**
 * @var dektrium\user\Module $module
 * @var dektrium\user\models\User $user
 * @var dektrium\user\models\Token $token
 * @var bool $showPassword
 */
?>

<div class="l-mail__block" style="margin: 0;padding: 130px 76px 135px;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;text-align: center;background: #FFF">
    <div class="l-mail__title" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;color: #005096;font-size: 30px;font-weight: 700;margin-bottom: 38px">
        Hello
    </div>
    <div class="l-mail__text" style="margin: 0;padding: 0;border: 0;font: inherit;vertical-align: baseline;box-sizing: border-box;color: #3b3b3b;font-size: 16px;margin-bottom: 65px;line-height: 1.4">
        <p style="box-sizing: border-box">
            Your account on <span class="l-mail__name" style="box-sizing: border-box;text-transform: capitalize;font-weight: 700"><?= Yii::$app->name ?></span> has been created.
                <?php if ($showPassword || $module->enableGeneratingPassword): ?>
                    We have generated a password for you: <strong><?= $user->password ?></strong>
                <?php endif ?>
        </p>
        
        <?php if ($token !== null): ?>
            <p style="box-sizing: border-box">
                In order to complete your registration, please click the link below
            </p>
            <p style="box-sizing: border-box">
                <?= Html::a(Html::encode($token->url), $token->url, ['class' => 'l-mail__link', 'style' => 'font-size: 16px; color: #005096; text-decoration: none;  font-weight: 700; margin: 0; padding: 0; border: 0; font: inherit; vertical-align: baseline; outline: 0;']) ?>
            </p>
            <p style="box-sizing: border-box">
                If you cannot click the link, please try pasting the text into your browser
            </p>
        <?php endif ?>
        <p style="box-sizing: border-box">
            If you did not make this request you can ignore this email
        </p>
    </div>
</div>
