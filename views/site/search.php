<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TourSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Search';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/allTours.css');
?>

<div class="l-action">
    <div class="l-mainContent">
        <div class="l-action__content--destination">
            <div class="l-action__formWrap">
                <?php $form = ActiveForm::begin([
                    'action' => '/search',
                    'method' => 'get',
                    'class' => 'c-searchForm'
                ]) ?>
                    <div class="l-action__formItem item">
                        <div class="c-searchForm__inputBlock">
                            <?= $form->field($tourSearch, 'name')->textInput(['class' => 'c-searchForm__input', 'placeholder' => 'Where are you going?', 'required' => true])->label(false) ?>
                            <div class="c-searchForm__img">
                                <svg class="c-searchForm__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 430.114 430.114">
                                    <path d="M356.208 107.051c-1.531-5.738-4.64-11.852-6.94-17.205C321.746 23.704 261.611 0 213.055 0 148.054 0 76.463 43.586 66.905 133.427v18.355c0 .766.264 7.647.639 11.089 5.358 42.816 39.143 88.32 64.375 131.136 27.146 45.873 55.314 90.999 83.221 136.106 17.208-29.436 34.354-59.259 51.17-87.933 4.583-8.415 9.903-16.825 14.491-24.857 3.058-5.348 8.9-10.696 11.569-15.672 27.145-49.699 70.838-99.782 70.838-149.104v-20.262c.001-5.347-6.627-24.081-7-25.234zm-141.963 92.142c-19.107 0-40.021-9.554-50.344-35.939-1.538-4.2-1.414-12.617-1.414-13.388v-11.852c0-33.636 28.56-48.932 53.406-48.932 30.588 0 54.245 24.472 54.245 55.06 0 30.587-25.305 55.051-55.893 55.051z"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="l-action__formItem l-action__formItem--button item">
                        <?= Html::submitButton('Search', ['class' => 'c-button']) ?>
                    </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>
<div class="l-tools">
    <div class="l-tools__content l-mainContent cf">
        <!--        <div class="l-tools__leftPart">
                    <div class="c-breadCrumbs">
                        <a class="c-breadCrumbs__link" href="#">
                            Europe
                        </a>
                        <div class="c-breadCrumbs__arrow">
                            <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 46.02 46.02">
                                <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                            </svg>
                        </div>
                    </div>
                    <div class="c-breadCrumbs">
                        <a class="c-breadCrumbs__link" href="#">
                            Europe
                        </a>
                        <div class="c-breadCrumbs__arrow">
                            <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 46.02 46.02">
                                <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                            </svg>
                        </div>
                    </div>
                    <div class="c-breadCrumbs">
                        <a class="c-breadCrumbs__link" href="#">
                            Europe
                        </a>
                        <div class="c-breadCrumbs__arrow">
                            <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 46.02 46.02">
                                <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                            </svg>
                        </div>
                    </div>
                    <div class="c-breadCrumbs">
                        <a class="c-breadCrumbs__link" href="#">
                            Europe
                        </a>
                        <div class="c-breadCrumbs__arrow">
                            <svg class="c-breadCrumbs__icon" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 46.02 46.02">
                                <path d="M14.757 46.02c-1.412 0-2.825-.521-3.929-1.569-2.282-2.17-2.373-5.78-.204-8.063L23.382 22.97 10.637 9.645C8.46 7.37 8.54 3.76 10.816 1.582c2.277-2.178 5.886-2.097 8.063.179l16.505 17.253c2.104 2.2 2.108 5.665.013 7.872L18.893 44.247c-1.123 1.177-2.626 1.773-4.136 1.773z"/>
                            </svg>
                        </div>
                    </div>
                </div>-->
        <div class="l-tools__rightPart">

        </div>
    </div>
</div>
<div class="l-destinations">
    <div class="l-destinations__content l-mainContent cf">
        <div class="l-destinations__search">
            <?= ListView::widget([
                'layout' => "{summary}\n{sorter}\n{items}\n{pager}",
                'dataProvider' => $dataProvider,
                'options' => [
                    'class' => 'l-destinations__list cf',
                ],
                'summary' => "
                    <div class='l-destinations__search--results'>
                        <span class='l-destination__search--textResults'>
                            Search results for '$tourSearch->name'
                        </span>
                        <span class='l-destination__search--textAmount'>
                            {totalCount} results found
                        </span>
                    </div>",
                'sorter' => [
                    'options' => [
                        'class' => 'c-sort__list',
                    ],
                    'linkOptions' => [
                        'class' => 'c-sort__text',
                    ],
                ],
                'itemOptions' => [
                    'class' => 'l-destinations__item',
                ],
                'itemView' => function ($model, $key, $index, $widget) {
                    return $this->render('/tour/_listTour',['tour' => $model]);
                },
                'emptyText' =>             
                    '<div class="c-empty">
                        <div class="c-empty__text">
                            There is no available tours 
                        </div>
                    </div>',
                'pager' => [
                    'options' => [
                        'class' => 'c-numberDots',
                    ],
                    'firstPageLabel' => '',
                    'prevPageCssClass' => 'c-numberDots__icon',
                    'lastPageLabel' => '',
                    'nextPageCssClass' => 'c-numberDots__icon',
                    'prevPageLabel' => '<svg class="c-numberDots__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.175 477.175">
                                            <path d="M145.188 238.575l215.5-215.5c5.3-5.3 5.3-13.8 0-19.1s-13.8-5.3-19.1 0l-225.1 225.1c-5.3 5.3-5.3 13.8 0 19.1l225.1 225c2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-4c5.3-5.3 5.3-13.8 0-19.1l-215.4-215.5z"></path>
                                        </svg>',
                    'nextPageLabel' => '<svg class="c-numberDots__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.175 477.175">
                                            <path d="M360.731 229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5-215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1c5.3-5.2 5.3-13.8.1-19z"></path>
                                        </svg>',
                    'activePageCssClass' => 'is-active',
                    'pageCssClass' => 'c-numberDots__item',
                    'linkOptions' => [
                        'class' => 'c-numberDots__text',
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>
