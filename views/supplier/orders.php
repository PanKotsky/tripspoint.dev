<?php

use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerCssFile('/css/supplierOrders.css');
$this->registerCssFile('/css/components/bootstrap-datetimepicker-standalone.css', ['depends' => kartik\date\DatePickerAsset::className()]);
?>

<div class="l-action l-action--textBlock">
    <div class="l-mainContent">
        <div class="l-action__content--textBlock">
            <div class="l-action__textBlock">
                <p class="l-action__text--white l-action__text--bold">
                    Orders!
                </p>
            </div>
        </div>
    </div>
</div>
<div class="l-profilePage">
    <div class="l-mainContent">
        <div class="l-profilePage__content cf">
            <!-- блок меню користувача -->
            <?=  $this->render('/layouts/_menu') ?>
            <!-- права частина -->
            <div class="l-profilePage__rightPart">
                <div class="c-orders">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $ordersSearch,
                        'showPageSummary' => false,
                        'tableOptions' => [
                            'class' => 'c-orders__table',
                        ],
                        'headerRowOptions' => [
                            'class' => 'c-orders__header',
                        ],
                        'rowOptions' => [
                            'class' => 'c-orders__rows',
                        ],
                        'columns' => [
                            [
                                'attribute' => 'tourId',
                                'width' => '173px',
                                'value' => function ($model, $key, $index, $widget) {
                                    return $model->tour->name;
                                },
                                'filterType' => GridView::FILTER_SELECT2,
                                'filter' => $toursList,
                                'filterWidgetOptions' => [
                                    'options' => ['style' => 'width:172px;'],
                                    'theme' => 'default',
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'dropdownCssClass' => 'ordersDropdown',
                                    ],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => 'Tour name',
                                    'width' => '100%',
                                    'dropdownAutoWidth' => true,
                                ],
                            ],
                            [
                                'attribute' => 'dateStartTour',
                                'width' => '100px',
                                'filterType' => GridView::FILTER_DATE,
                                'filterWidgetOptions' => [
                                    'options' => ['style' => 'width:75%;'],
                                    'type' => 3,
                                    'removeButton' => '
                                        <span class="kv-date-remove orders__dateRemove" title="Clear field">
                                        ×
                                        </span>',
                                    'options' => [
                                      'placeholder' => 'Start date',
                                    ],
                                    'pluginOptions' => [
                                        'format' => 'd/m/yyyy',
                                        'placeholder' => 'ssss',
                                    ],
                                ],
                            ],
                            [
                                'attribute' => 'languageId',
                                'width' => '90px',
                                'value' => function ($model, $key, $index, $widget) {
                                    return $model->guide->language;

                                },
                                'filterType' => GridView::FILTER_SELECT2,
                                'filter' => $guidesList,
                                'filterWidgetOptions' => [
                                    'theme' => 'default',
                                    'options' => ['style' => 'width:100%; font-size: 12px;'],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'dropdownCssClass' => 'ordersDropdown'
                                    ],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => 'Guide'
                                ],
                            ],
                            [
                                'attribute' => 'travellers',
                                'width' => '80px',
                                'filterType' => GridView::FILTER_SELECT2,
                                'filter' => [
                                    1 => 'Travellers',
                                    2 => 'Groups'
                                ],
                                'filterWidgetOptions' => [
                                    'theme' => 'default',
                                    'options' => ['style' => 'width:100%;'],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'dropdownCssClass' => 'ordersDropdown'
                                    ],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => 'Travellers'
                                ],
                            ],
                            [
                                'attribute' => 'typeId',
                                'value' => function ($model, $key, $index, $widget) {
                                    return $model->tour->type->type;

                                },
                                'filterType' => GridView::FILTER_SELECT2,
                                'filter' => $typesList,
                                'filterWidgetOptions' => [
                                    'theme' => 'default',
                                    'options' => ['style' => 'width:100%;'],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'dropdownCssClass' => 'ordersDropdown'
                                    ],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => 'Tour Type'
                                ],
                            ],
                            [
                                'class' => 'kartik\grid\EditableColumn',
                                'attribute' => 'orderStatus',
                                'width' => '100px',
                                'format' => 'raw',
                                'value' => function ($model, $key, $index, $widget) {
                                    if ($model->orderStatus == 0) {
                                        return '<svg class="orders__checked orders__checked--reject" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44 44">
                                                  <path d="m22,0c-12.2,0-22,9.8-22,22s9.8,22 22,22 22-9.8 22-22-9.8-22-22-22zm3.2,22.4l7.5,7.5c0.2,0.2 0.3,0.5 0.3,0.7s-0.1,0.5-0.3,0.7l-1.4,1.4c-0.2,0.2-0.5,0.3-0.7,0.3-0.3,0-0.5-0.1-0.7-0.3l-7.5-7.5c-0.2-0.2-0.5-0.2-0.7,0l-7.5,7.5c-0.2,0.2-0.5,0.3-0.7,0.3-0.3,0-0.5-0.1-0.7-0.3l-1.4-1.4c-0.2-0.2-0.3-0.5-0.3-0.7s0.1-0.5 0.3-0.7l7.5-7.5c0.2-0.2 0.2-0.5 0-0.7l-7.5-7.5c-0.2-0.2-0.3-0.5-0.3-0.7s0.1-0.5 0.3-0.7l1.4-1.4c0.2-0.2 0.5-0.3 0.7-0.3s0.5,0.1 0.7,0.3l7.5,7.5c0.2,0.2 0.5,0.2 0.7,0l7.5-7.5c0.2-0.2 0.5-0.3 0.7-0.3 0.3,0 0.5,0.1 0.7,0.3l1.4,1.4c0.2,0.2 0.3,0.5 0.3,0.7s-0.1,0.5-0.3,0.7l-7.5,7.5c-0.2,0.1-0.2,0.5 3.55271e-15,0.7z"/>
                                                </svg>';
                                    } elseif ($model->orderStatus == 1) {
                                        return '<svg class="orders__checked" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 426.667 426.667">
                                                    <path d="M213.333,0C95.518,0,0,95.514,0,213.333s95.518,213.333,213.333,213.333
                                                         c117.828,0,213.333-95.514,213.333-213.333S331.157,0,213.333,0z M174.199,322.918l-93.935-93.931l31.309-31.309l62.626,62.622
                                                         l140.894-140.898l31.309,31.309L174.199,322.918z"></path>
                                                </svg>';
                                    }
                                },
                                'filterType' => GridView::FILTER_SELECT2,
                                'filter' => [
                                    0 => 'rejected',
                                    1 => 'confirmed'
                                ],
                                'filterWidgetOptions' => [
                                    'theme' => 'default',
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'dropdownCssClass' => 'ordersDropdown'
                                    ],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => 'Status'
                                ],
                                'editableOptions'=>[
                                    'asPopover' => false,
                                    'formOptions' => [
                                        'action' => '/order/setstatus',
                                    ],
                                    'containerOptions' => [
                                      'class' => 'c-orders__conditionals'
                                    ],
                                    // styling <div class="panel">
                                    'inlineSettings' => [
                                        'closeButton' => '<button class="c-button c-button--reset c-orders__close kv-editable-close">Close</button>'
                                    ],
                                    'buttonsTemplate' => '{submit}',
                                    // html options for submit button
                                    'submitButton' => [
                                        'class' => 'c-orders__submit c-button',
                                        'icon' => '<span>Apply</span>'
                                    ],
                                    'inputType' => 'radioList',
                                    // styling radio list
                                    'options' => [
                                        'class' => 'c-orders__inputs'
                                    ],
                                    'data' => [
                                        0 => 'reject',
                                        1 => 'confirm',
                                    ]
                                ],
                            ],
                            [
                                'attribute' => 'totalPrice',
                                'width' => '100px',
                                'mergeHeader' => true,
                            ],
                            [
                                'class' => 'kartik\grid\ExpandRowColumn',
                                'width' => '60px',
                                'expandIcon' => '<span class="c-orders__details">details</span>',
                                'collapseIcon' => '<span>hide</span>',
                                'value' => function ($model, $key, $index, $column) {
                                    return GridView::ROW_COLLAPSED;
                                },
                                'detail' => function ($model, $key, $index, $column) {
                                    return Yii::$app->controller->renderPartial('_orderDetails', ['order' => $model]);
                                },
                                'expandOneOnly' => true
                            ],
                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'template' => '{chat}',
                                'urlCreator' => function($action, $model, $key, $index) {
                                    if ($action === 'chat') {
                                        return $url = Url::to(['messages/dialogs', 'ui' => $model->userId, 'ti' => $model->tourId]);
                                    }
                                },
                                'buttons' => [
                                    'chat' => function ($url, $model) {
                                        return Html::a('<svg class="c-orders__message" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14">
                    <path d="M7 9L5.268 7.484.316 11.729c.18.167.423.271.691.271h11.986c.267 0 .509-.104.688-.271L8.732 7.484 7 9z"></path>
                    <path d="M13.684 2.271c-.18-.168-.422-.271-.691-.271H1.007c-.267 0-.509.104-.689.273L7 8l6.684-5.729zM0 2.878v8.308l4.833-4.107m4.334 0L14 11.186V2.875"></path>
                </svg>', $url);
                                    },
                                ],
                                'contentOptions' => [
                                  'class' => 'c-orders__messageWrapper'
                                ],
                            ],
                        ],
                        'bootstrap' => false,
                        'perfectScrollbar' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'loadingCssClass' => true,
                        ],
                        'resizableColumns' => false,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

