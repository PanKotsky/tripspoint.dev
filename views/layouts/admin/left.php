<aside class="main-sidebar">
    <section class="sidebar">
        <? if (Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->getId())['admin']) {
            echo dmstr\widgets\Menu::widget([
                'id' => ['admin-form'],
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Admin Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Static pages', 'icon' => 'file-code-o', 'url' => ['/admin/static-pages']],
                    ['label' => 'Users', 'icon' => 'file-code-o', 'url' => ['/admin/users']],
                ],
            ]);
        } ?>
        <?= dmstr\widgets\Menu::widget([
            'id' => ['destination-form'],
            'options' => ['class' => 'sidebar-menu'],
            'items' => [
                ['label' => 'Destination Menu', 'options' => ['class' => 'header']],
                ['label' => 'Suppliers', 'icon' => 'file-code-o', 'url' => ['/admin/suppliers']],
                ['label' => 'Tours', 'icon' => 'file-code-o', 'url' => ['/admin/tours']],
                ['label' => 'Orders', 'icon' => 'file-code-o', 'url' => ['/admin/orders']],
                ['label' => 'Support Messages', 'icon' => 'file-code-o', 'url' => ['/admin/support']],
            ],
        ]) ?>
    </section>
</aside>
