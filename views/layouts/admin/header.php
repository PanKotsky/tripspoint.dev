<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">
    <?= Html::a('<span class="logo-lg">Tripspoint</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    <nav class="navbar navbar-static-top" role="navigation">
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?= Url::to(['/admin/index']) ?>">
                        <? if (Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->getId())['admin']) { ?>
                            <span class="hidden-xs">Administrator</span>
                        <? } else if (Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->getId())['destination']) { ?>
                            <span class="hidden-xs">Destination Manager</span>
                        <? } ?>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>