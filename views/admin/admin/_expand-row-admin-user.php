<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 26.09.2017
 * Time: 16:23
 */

use yii\widgets\DetailView;

//debug($model->providerDocs);
//die;


echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'username',
        'gender',
        'birthday',
        'country',
        'region',
        'city',
        'zipcode',
        'address',
        'src_avatar'
    ]
]);

//foreach ($model->providerDocs as $docs){
//    echo "<img class='adminSupplier__img' src='/uploads/docs/".$model->userId."/".$docs->srcDoc."' weight='300px'>";
//}