<?php
/* @var $this yii\web\View */

use app\assets\WithBootstrapAsset;
WithBootstrapAsset::register($this);
?>

<div class="site-index">
    <div class="adminMain__title">
        Welcome to TripsPoint.com
        <br/>
        Admin Panel
    </div>
    <div class="adminMain__text">
        Please, use the left sidebar for navigation through Admin Panel.
    </div>
</div>

