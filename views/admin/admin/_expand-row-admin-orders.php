<?php

use yii\widgets\DetailView;

//debug($model->providerDocs);
//die;


echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'totalSenior',
        'totalAdult',
        'totalChild',
        'totalInfant',
        'dateStartTour',
        'leadFirstname',
        'leadLastname',
        'leadPhone',
        'leadEmail',
    ]
]);

