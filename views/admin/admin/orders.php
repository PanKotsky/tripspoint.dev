<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 08.09.2017
 * Time: 19:29
 */

use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\WithBootstrapAsset;

WithBootstrapAsset::register($this);

//debug($dataProvider->getModels());
//die;

$gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'cartId',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(\app\models\Orders::find()->select(['orderId','cartId'])->asArray()->all(), 'cartId', 'cartId'),
//        'filter' => $orderIdList,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'Cart ID'],
        'vAlign'=>'middle',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'nameTour',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $tourList,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'Tour ID'],
        'vAlign'=>'middle',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'providerOrder',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $supplierList,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'supplier'],
        'vAlign'=>'middle',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'customer',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $customerList,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'customer'],
        'vAlign'=>'middle',
    ],
    [
        'class'=>'kartik\grid\DataColumn',
        'attribute'=>'dateBuy',
        'value' => 'dateBuy',
        'format' =>  ['date', 'dd/M/yyyy'],
//        'xlFormat'=>'dd-M-yyyy',
//        'filterType' => GridView::FILTER_DATE,
        'filterType' => GridView::FILTER_DATE,
        'filter' => DatePicker::widget([
            'name' => 'dp_3',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'value' => 'dateBuyList',
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'dd/M/yyyy'
            ]
        ]),
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'width'=>'9%',
        'headerOptions'=>['class'=>'kv-sticky-column'],
        'contentOptions'=>['class'=>'kv-sticky-column'],
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'payPrice',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $payPriceList,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'pay price'],
        'vAlign'=>'middle',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'totalPrice',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => $totalPriceList,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['allowClear'=>true],
        ],
        'filterInputOptions'=>['placeholder'=>'total price'],
        'vAlign'=>'middle',
    ],
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($dataProviders, $key, $index, $column) {
            return Yii::$app->controller->renderPartial('_expand-row-admin-orders', ['model'=>$dataProviders]);
//            return 1;
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true,
        'expandTitle' => 'details',
        'collapseTitle' => 'details',
        'expandIcon' => '<span>Open<span>',
        'collapseIcon' => '<span>Close<span>',
//        'options' => ['style' => 'width: 3%'],
        'vAlign'=>'middle',
    ],
    ['class' => 'yii\grid\ActionColumn',
        'template' => '{approve}{cancel}',
        'header' => 'Actions',
        'buttons' => [

            'approve' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-ok-sign"></span>', $url, [
                    'title' => Yii::t('app', 'approve'),
                ]);
            },
            'cancel' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-remove-sign"></span>', $url, [
                    'title' => Yii::t('app', 'cancel'),
                ]);
            },
        ],
        'urlCreator' => function ($action, $model, $key, $index) {

            if ($action === 'approve') {
                $url = Url::to(['admin/approve-order', 'oid' => $model->orderId]);
                return $url;
            }
            if ($action === 'cancel') {
                $url = Url::to(['admin/cancel-order', 'oid' => $model->orderId]);
                return $url;
            }
        }
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'header' => 'Message',
        'headerOptions'=>['style' => 'width:4%'],
        'template' => '{message}',
        'buttons' => [
            'message' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-envelope"></span>', $url, [
                    'title' => Yii::t('app', 'Message to supplier'),
                ]);
            },
        ],
        'urlCreator' => function ($action, $model, $key, $index) {

            if ($action === 'message') {
                $url = Url::to(['admin/approve-supplier', 'uid' => $model->userId]);
                return $url;
            }
        }
    ]
];

echo GridView::widget([
    'dataProvider'=> $dataProvider,
    'filterModel' => $searchModel,
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
    'columns' => $gridColumns,
    'responsive'=>true,
    'hover'=>true,
    'resizableColumns'=>true,
]);