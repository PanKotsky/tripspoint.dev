<?php
use yii\helpers\Url;

$this->title = 'Page not found';
$this->registerCssFile('/css/404.css');
?>

<div class="l-404">
    <div class="l-mainContent">
        <div class="l-404__content">
            <div class="l-404__Block">
                <div class="l-404__textBlock">
                    <p class="l-404__textBig">
                        Oops!
                    </p>
                    <p class="l-404__textSmall">
                        Seems like you're lost
                    </p>
                </div>
                <div class="l-404__buttonWrapper">
                    <a href="<?= Url::to(['/site/index']) ?>" class="c-button">
                        Back to main
                    </a>
                </div>
            </div>
            <div class="l-404__error">
                <div class="l-404__errorText">
                    404
                </div>
                <div class="l-404__plane"></div>
                <div class="l-404__lines">
                    <img src="/img/404/lines.png">
                </div>
            </div>
        </div>
    </div>
</div>