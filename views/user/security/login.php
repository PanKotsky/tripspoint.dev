<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
//use yii\authclient\widgets\AuthChoice;
use app\components\AuthChoice;
//use dektrium\user\models\LoginForm;
use yii\helpers\Url;
use app\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module $module
 */

$this->registerCssFile('/css/logIn.css');
$this->title = Yii::t('user', 'Log in');
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="l-action--empty"></div>
<div class="l-tools">
    <div class="l-tools__header  l-mainContent">
        <div class="l-tools__form">
            <div class="c-logIn">
                <div class="c-logIn__header">Log In</div>
                <div class="c-logIn__body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'fieldConfig' => [
                            'template' => "<div>{input}</div><div class='help-block'>{error}\n{hint}</div>",
                        ],
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'validateOnBlur' => false,
                        'validateOnType' => false,
                        'validateOnChange' => false,
                    ]) ?>
                        <div class="c-logIn__inputBlock">
                            <?php if ($module->debug): ?>
                                <?= $form->field($model, 'login', [
                                    'inputOptions' => [
                                        'autofocus' => 'autofocus',
                                        'class' => 'c-logIn__input form-control',
                                        'tabindex' => '1']])->dropDownList(LoginForm::loginList());
                                ?>

                            <?php else: ?>
                                <?= $form->field($model, 'login',
                                    ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'c-logIn__input form-control', 'tabindex' => '1']]
                                )->textInput(['placeholder' => 'E-mail']);
                                ?>

                            <?php endif ?>
                            <svg class="c-logIn__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14">
                                <path d="M7 9L5.268 7.484.316 11.729c.18.167.423.271.691.271h11.986c.267 0 .509-.104.688-.271L8.732 7.484 7 9z"/>
                                <path d="M13.684 2.271c-.18-.168-.422-.271-.691-.271H1.007c-.267 0-.509.104-.689.273L7 8l6.684-5.729zM0 2.878v8.308l4.833-4.107m4.334 0L14 11.186V2.875"/>
                            </svg>
                        </div>
                        <div class="c-logIn__inputBlock">
                            <?php if ($module->debug): ?>
                                <div class="alert alert-warning">
                                    <?= Yii::t('user', 'Password is not necessary because the module is in DEBUG mode.'); ?>
                                </div>
                            <?php else: ?>
                                <?= $form->field(
                                    $model,
                                    'password',
                                    ['inputOptions' => ['class' => 'c-logIn__input form-control', 'tabindex' => '2']])
                                    ->passwordInput(['placeholder' => 'Password'])
                                    ?>
                            <?php endif ?>
                            <svg class="c-logIn__icon" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 486.733 486.733">
                                <path d="M403.88 196.563h-9.484v-44.388c0-82.099-65.151-150.681-146.582-152.145-2.225-.04-6.671-.04-8.895 0C157.486 1.494 92.336 70.076 92.336 152.175v44.388h-9.485c-14.616 0-26.538 15.082-26.538 33.709v222.632c0 18.606 11.922 33.829 26.539 33.829H403.88c14.616 0 26.539-15.223 26.539-33.829V230.272c0-18.626-11.922-33.709-26.539-33.709zM273.442 341.362v67.271c0 7.703-6.449 14.222-14.158 14.222H227.45c-7.71 0-14.159-6.519-14.159-14.222v-67.271c-7.477-7.36-11.83-17.537-11.83-28.795 0-21.334 16.491-39.666 37.459-40.513 2.222-.09 6.673-.09 8.895 0 20.968.847 37.459 19.179 37.459 40.513-.002 11.258-4.355 21.435-11.832 28.795zm58.444-144.799H154.847v-44.388c0-48.905 39.744-89.342 88.519-89.342 48.775 0 88.521 40.437 88.521 89.342v44.388z"/>
                            </svg>
                        </div>
                        <div class="c-logIn__passwordBlock cf">
                            <div class="c-logIn__rememberBlock">
                                    <?php  $checkboxTemplate = '<span class="c-logIn__checkIn"></span>{input}<span class="c-logIn__label">{label}</span>'; ?>
                                    <?= $form->field($model, 'rememberMe', ['template' => "<span class=\"\"></span><span class=\"c-logIn__label\">{input}{label}</span>"])->checkbox(['tabindex' => '3', 'class' => 'c-logIn__checkIn c-logIn__checkbox'])->label('');?>
                            </div>
                            <div class="c-logIn__forgotBlock">
                                <a href="<?=Url::to(['/user/recovery/request'])?>" class="c-logIn__forgotLink">Forgot your password?</a>
                            </div>
                        </div>
                        <div class="c-logIn__buttonWrap">
                            <?= Html::submitButton(
                                Yii::t('user', strtoupper('Log in')),
                                ['class' => 'c-button', 'tabindex' => '4']
                            ) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                    <div class="c-logIn__separator">or</div>
                    <!-- Віджет реєстрації через соцмережі, верстка знаходиться всередині віджета
                             Якщо додається соцмережа у віджеті в методі clientLink() додати іконку соцмережі-->
                    <?= AuthChoice::widget([
                        'baseAuthUrl' => ['/user/security/auth']
                    ]) ?>
                </div>
                <div class="c-logIn__footer">
                    <div class="c-logIn__question">Don't have an account?</div>
                    <a href="<?=Url::to(['/user/register'])?>" class="c-logIn__question c-logIn__question--link">Sign Up!</a>
                </div>
            </div>
        </div>
    </div>
</div>