<?php

use yii\helpers\Html;

$this->registerCssFile('/css/confirmSignUp.css');
$this->title = Yii::t('user', 'Sign up');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="l-action--empty"></div>
<div class="l-tools">
    <div class="l-tools__header  l-mainContent">
        <div class="l-tools__form">
            <div class="c-confirmSignUp">
                <div class="c-confirmSignUp__body">
                    <? if (\Yii::$app->session->hasFlash('success')) { ?>
                        <p class="c-confirmSignUp__text"><?= \Yii::$app->session->getFlash('success') ?></p>
                        <div class="c-confirmSignUp__buttonWrap">
                            <?= Html::a('Log In', ['/user/login'], ['class' => 'c-button']) ?>
                        </div>
                    <? } else if (\Yii::$app->session->hasFlash('danger')) { ?>
                        <p class="c-confirmSignUp__text"><?= \Yii::$app->session->getFlash('danger') ?></p>
                        <div class="c-confirmSignUp__buttonWrap">
                            <?= Html::a('Back to main', ['/site/index'], ['class' => 'c-button']) ?>
                        </div>
                    <? } ?>
                </div>
            </div>
        </div>
    </div>
</div>



