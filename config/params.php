<?php

return [
//    'adminEmail' => 'java.greenweb@gmail.com',
    'adminEmail' => 'test-smtp@tripspoint.com',
    'supportEmail' => 'java.greenweb@gmail.com',
    // percentage of price for one adult for required booking deposit !!!
    'depositPercentage' => 20,
];
