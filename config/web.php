<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'tripspoint',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'name' => 'tripspoint',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'eUFaB3l8z3AIna5KrIaWekm8xia9oEaa',
            'baseUrl' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],

        'profile' => [
            'class' => 'app\models\Profile',
        ],

        'authClientCollection' => [
            'class'   => \yii\authclient\Collection::className(),
            'clients' => [
                'facebook' => [
                    'class'        => 'dektrium\user\clients\Facebook',
                    'clientId'     => '417277921980055',
                    'clientSecret' => '7b9986485951cd8235e0b47cd136dc42',
                ],
                'google' => [
                    'class'        => 'dektrium\user\clients\Google',
                    'clientId'     => '62939074839-dkp9tgkcc94do73iqk9hcehomoeb8986.apps.googleusercontent.com',
                    'clientSecret' => 'G4EYBuFSCg1mw0OP4EOM3nLD',
                ]

                // here is the list of clients you want to use
                // you can read more in the "Available clients" section
            ],
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                // LOCALHOST VERSION
//                'host' => 'smtp.gmail.com',  // Вказати мейл для сервіса
//                'username' => 'java.greenweb@gmail.com',            // Вказати юзернейм для мейл
//                'password' => 'dareloki',                     // Вказати пасс для мейл
//                'port' => '587',
//                'encryption' => 'tls',
//                'streamOptions' => [
//                    'ssl' => [
//                        'allow_self_signed' => true,
//                        'verify_peer' => false,
//                        'verify_peer_name' => false,
//                    ],
//                ],
                // PRODUCTION VERSION
                'host' => 'premium22.web-hosting.com',  // Вказати мейл для сервіса
                'username' => 'test-smtp@tripspoint.com',            // Вказати юзернейм для мейл
                'password' => '@l2CRYVpEF$jV57',                     // Вказати пасс для мейл
                'port' => '587',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
//                'admin/<action:\w+>/<id:\d+>' => 'admin/admin/<action>',
//                'admin/<action:\w+(-){0,1}\w+>' => 'admin/admin/<action>',
                'admin' => 'admin/admin/index',
                'admin/<action:[0-9a-zA-Z_-]+>' => 'admin/admin/<action>',
                'supplier' => 'supplier/index',
                'tour' => 'tour/index',
                '<action>'=>'site/<action>',
//                'user/profile/bookings' => 'profile/bookings',
//                'profile/bookings' => 'user/bookings',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
//                'user/forgot' => 'recovery/request',
                'user/register' => 'registration/register',
                'user/index' => 'profile/show',

//                'admin/admin/index' => 'admin/index'
//                'user/login' => 'security/login',
//                'user/logout' => 'security/logout',
//                'user/settings' => 'settings/profile',
//                'user/update-avatar' => 'settings/update-avatar'
//                '<action>'=>'site/<action>'
            ],
        ],

        'mymessages' => [
            //Обязательно
            'class'    => 'app\components\MyMessages',
            //не обязательно
            //класс модели пользователей
            //по-умолчанию \Yii::$app->user->identityClass
            'modelUser' => 'app\models\User',
            //имя контроллера где разместили action
            'nameController' => 'messages',
            //не обязательно
            //имя поля в таблице пользователей которое будет использоваться в качестве имени
            //по-умолчанию username
            'attributeNameUser' => 'username',
            //не обязательно
            //можно указать роли и/или id пользователей которые будут видны в списке контактов всем кто не подпадает
            //в эту выборку, при этом указанные пользователи будут и смогут писать всем зарегестрированным пользователям
//            'admins' => ['admin', 7],
            //не обязательно
            //включение возможности дублировать сообщение на email
            //для работы данной функции в проектк должна быть реализована отправка почты штатными средствами фреймворка
            'enableEmail' => true,
            //задаем функцию для возврата адреса почты
            //в качестве аргумента передается объект модели пользователя
            'getEmail' => function($user_model) {
                return $user_model->email;
            },
            //задаем функцию для возврата лого пользователей в списке контактов (для виджета cloud)
            //в качестве аргумента передается id пользователя
            'getLogo' => function($user_id) {
                return '\img\ghgsd.jpg';
            },
            //указываем шаблоны сообщений, в них будет передаваться сообщение $message
            'templateEmail' => [
                'html' => 'private-message-text',
                'text' => 'private-message-html'
            ],
            //тема письма
            'subject' => 'Private message'
        ],

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views/layouts' => '@app/views/layouts',
                    '@dektrium/user/views' => '@app/views/user',
                    '@vendor/dmstr/yii2-adminlte-asset/example-views/phundament/app' => '@app/views/admin',
//                    '@app/views/admin' => '@vendor/dmstr/yii2-adminlte-asset/example-views/phundament/app'
                ],
            ],
        ],

        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js'=>[]
                ],
                //'vision\messages\assets\MessageAssets' => false,
                'vision\messages\assets\BaseMessageAssets' => false,
                'vision\messages\assets\CloadAsset' => false,
                'vision\messages\assets\MessageKushalpandyaAssets' => false,
                'vision\messages\assets\PrivateMessPoolingAsset' => false,
                'vision\messages\assets\SortElementsAsset' => false,
                'vision\messages\assets\TinyscrollbarAsset' => false,

            ],
        ],

        'socialShare' => [
            'class' => \ymaker\social\share\configurators\Configurator::class,
            'socialNetworks' => [
                'facebook' => [
                    'class' => \ymaker\social\share\drivers\Facebook::class,
                    'label' =>
                        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 96 96">
                            <path d="M48,0C21.49,0,0,21.49,0,48c0,26.511,21.49,48,48,48s48-21.489,48-48C96,21.49,74.51,0,48,0zM59.369,33.17h-7.217c-0.854,0-1.805,1.121-1.805,2.623v5.211h9.021v7.428h-9.021v22.306h-8.52V48.432h-7.723v-7.428h7.723v-4.372c0-6.269,4.352-11.368,10.324-11.368h7.217L59.369,33.17L59.369,33.17z"/>
                        </svg>',
                ],
                'twitter' => [
                    'class' => \ymaker\social\share\drivers\Twitter::class,
                    'label' =>
                        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 96 96">
                            <path d="M48,0C21.488,0,0,21.49,0,48c0,26.511,21.488,48,48,48c26.51,0,48-21.489,48-48C96,21.49,74.51,0,48,0z M67.521,39.322c0.02,0.406,0.027,0.814,0.027,1.224c0,12.493-9.51,26.899-26.898,26.899c-5.338,0-10.307-1.566-14.49-4.249c0.738,0.089,1.49,0.133,2.254,0.133c4.43,0,8.506-1.511,11.742-4.048c-4.137-0.075-7.629-2.809-8.832-6.564c0.578,0.109,1.17,0.17,1.779,0.17c0.861,0,1.697-0.116,2.49-0.332c-4.324-0.869-7.584-4.689-7.584-9.271c0-0.04,0-0.079,0.002-0.118c1.273,0.708,2.732,1.133,4.281,1.183c-2.537-1.696-4.205-4.589-4.205-7.87c0-1.732,0.465-3.355,1.279-4.752c4.662,5.72,11.629,9.483,19.486,9.878c-0.162-0.692-0.244-1.414-0.244-2.155c0-5.221,4.232-9.453,9.453-9.453c2.719,0,5.176,1.148,6.9,2.985c2.154-0.424,4.178-1.21,6.004-2.294c-0.707,2.207-2.205,4.061-4.156,5.23c1.912-0.229,3.734-0.736,5.43-1.488C70.973,36.324,69.369,37.99,67.521,39.322z"/>
                        </svg>',
                ],
                'googlePlus' => [
                    'class' => \ymaker\social\share\drivers\GooglePlus::class,
                    'label' =>
                        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 96 96">
                            <path d="M43.484,34.425c-1.209-4.248-3.158-5.505-6.191-5.505c-0.328,0-0.656,0.045-0.973,0.135c-1.312,0.369-2.355,1.467-2.938,3.094c-0.592,1.653-0.633,3.373-0.117,5.34c0.922,3.501,3.412,6.043,5.924,6.043c0.33,0,0.656-0.045,0.971-0.134C42.904,42.625,44.623,38.434,43.484,34.425z M48,0C21.49,0,0,21.49,0,48c0,26.511,21.49,48,48,48c26.512,0,48-21.489,48-48C96,21.49,74.512,0,48,0z M45.234,68.609c-3.014,1.465-6.262,1.623-7.518,1.623c-0.238,0-0.377-0.006-0.398-0.006c0,0-0.09,0.002-0.244,0.002c-1.957,0-11.713-0.45-11.713-9.336c0-8.731,10.623-9.412,13.879-9.412l0.086,0.001c-1.881-2.509-1.49-5.044-1.49-5.044c-0.166,0.012-0.406,0.023-0.703,0.023c-1.227,0-3.59-0.196-5.623-1.508c-2.486-1.6-3.746-4.324-3.746-8.098c0-10.652,11.633-11.084,11.748-11.086h11.619v0.252c0,1.301-2.332,1.548-3.926,1.766c-0.537,0.074-1.621,0.188-1.928,0.346c2.941,1.568,3.418,4.039,3.418,7.721c0,4.188-1.641,6.404-3.381,7.961c-1.074,0.962-1.924,1.722-1.924,2.732c0,0.992,1.164,2.014,2.51,3.195c2.207,1.939,5.23,4.593,5.23,9.052C51.131,63.408,49.146,66.71,45.234,68.609z M70.639,47.955h-7.578v7.579H58.26v-7.579h-7.578v-4.8h7.578v-7.579h4.801v7.579h7.578V47.955z M39.113,53.758c-0.26,0-0.521,0.01-0.783,0.026c-2.223,0.164-4.268,0.995-5.756,2.344c-1.469,1.331-2.219,3.003-2.111,4.708c0.223,3.549,4.039,5.635,8.674,5.297c4.564-0.332,7.604-2.954,7.381-6.508C46.307,56.279,43.402,53.758,39.113,53.758z"/>
                        </svg>',
                ],
                'pinterest' => [
                    'class' => \ymaker\social\share\drivers\Pinterest::class,
                    'label' =>
                        '<svg xmlns="http://www.w3.org/2000/svg"viewBox="0 0 96 96">
                            <path d="M48,0C21.488,0,0,21.49,0,48c0,26.511,21.488,48,48,48c26.51,0,48-21.489,48-48C96,21.49,74.51,0,48,0z M51.283,59.383c-3.078-0.238-4.369-1.762-6.779-3.227c-1.326,6.955-2.947,13.624-7.748,17.107c-1.482-10.514,2.176-18.409,3.873-26.792c-2.895-4.874,0.35-14.685,6.457-12.267c7.514,2.973-6.508,18.121,2.904,20.014c9.83,1.975,13.842-17.054,7.748-23.242c-8.807-8.934-25.633-0.203-23.564,12.59c0.504,3.128,3.734,4.076,1.291,8.393c-5.637-1.25-7.318-5.693-7.102-11.621c0.348-9.699,8.717-16.491,17.107-17.431c10.613-1.188,20.576,3.896,21.951,13.88C68.971,48.057,62.631,60.262,51.283,59.383z"/>
                        </svg>',
                ],
            ],
        ],
    ],

    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'modelMap' => [
                'User'    => 'app\models\User',
                'Profile' => 'app\models\Profile',
            ],
            'controllerMap' => [
                'recovery' => 'app\controllers\RecoveryController',
                'security' => 'app\controllers\SecurityController',
//                'admin' => 'app\controllers\SecurityController',
                'admin' => 'app\controllers\admin\AdminController',
                /*'admin' => [
                    'class'  => 'app\controllers\admin\AdminController',
//                    'layout' => '@app/views/layout/admin/main',
                ],*/
                'registration' => 'app\controllers\RegistrationController',
                'profile' => 'app\controllers\ProfileController',
                'settings' => 'app\controllers\SettingsController'
            ],
            'enableUnconfirmedLogin' => true,
            'enablePasswordRecovery' => true,
            'rememberFor' => 2592000,
            'confirmWithin' => 21600,
            'recoverWithin' => 21600,
            'cost' => 12,
            'admins' => ['admin'],
            'mailer' => [
                'viewPath' => '@app/mailer',
            ]
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
    ],

    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
//    $config['modules']['debug']['allowedIPs'] = ['192.168.*'];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
