<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 22.05.2017
 * Time: 18:06
 */

namespace app\components;

use app\models\MsgFiles;
use vision\messages\components\MyMessages as BaseMyMessage;
use vision\messages\models\Messages;
use vision\messages\exceptions\ExceptionMessages;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\UploadedFile;
use Yii;

class MyMessages extends BaseMyMessage
{
    const EVENT_SEND   = 'sendMessage';
    const EVENT_STATUS = 'changeStatus';

    /** @var ActiveRecord */
    public $modelUser;

    /** @var string */
    public $attributeNameUser = 'username';

    /** @var string */
    public $nameController;

    /** @var boolean */
    public $enableEmail = true;

    /** @var boolean */
    public $admins = null;

    /** @var boolean */
    public $isSystem = false;

    /** @var callable */
    public $getEmail = null;

    /** @var callable */
    public $getLogo = null;

    /** @var array */
    public $templateEmail = [];

    /** @var string */
    public $subject = 'Private message';

    /** @var string */
    protected $userTableName;

    /** @var array */
    protected $adminIds = null;


    public static function getMessageComponent()
    {
        return \Yii::$app->mymessages;
    }


    public function init(){
        if(!$this->modelUser) {
            $this->modelUser = \Yii::$app->user->identityClass;
        }
        $this->userTableName = call_user_func([$this->modelUser, 'tableName']);
    }

    /**
     * @param $whom_id
     * @param $message
     * @param bool $sendEmail
     * @param integer $tourId
     * @return array|null
     */
    public function sendMessage($whom_id, $message, $sendEmail = false, $tourId = null) {
        $result = null;

        if($this->admins && !in_array(\Yii::$app->user->id, $this->getAdminIds())) {
            if(!in_array($whom_id, $this->getAdminIds())){
                $whom_id = $this->getAdminIds();
            }
        }

        if(!is_numeric($whom_id) && is_string($whom_id)) {
            $ids = $this->getUsersByRoles($whom_id);
            return $this->sendMessage($ids, $message, $sendEmail, $tourId);
        } elseif (is_array($whom_id)) {
            $result = $this->_sendMessages($whom_id, $message, $sendEmail, $tourId);
        } else {
            $result = $this->_sendMessage($whom_id, $message, $sendEmail, $tourId);
        }
        return $result;
    }



    /**
     * Method to sendMessage.
     *
     * @param $whom_id
     * @param $message
     * @param bool $send_email
     * @param integer $tourId
     * @return array
     */
    protected function _sendMessage($whom_id, $message, $send_email = false, $tourId = null) {
        $model = new Messages();
        $model->from_id = $this->getIdCurrentUser();
        $model->whom_id = $whom_id;
        $model->tourId = $tourId;
        $model->message = Html::encode($message);
        if($this->enableEmail && $send_email) {
            $this->_sendEmail($whom_id, $message);
        }

        return $this->saveData($model, self::EVENT_SEND);
    }

    /**
     * Method to _sendEmail.
     *
     * @param $whom_id
     * @param $message
     *
     * @throws ExceptionMessages
     *
     * @return boolean, array
     */
    protected function _sendEmail($whom_id, $message) {
        if(!is_callable($this->getEmail)) {
            throw new ExceptionMessages('Email not send. Set in config "getEmail" to callable func.');
        }
        if(!isset($this->templateEmail['html'], $this->templateEmail['text'])) {
            throw new ExceptionMessages('Email not send. Set in config "templateEmail".');
        }
        $user = $this->getUser($whom_id);
        if($user) {
            $email = call_user_func($this->getEmail, $user);
        }
        if(!empty($email)) {
            return \Yii::$app->mailer
                ->compose(['message' => $message])
                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' private Message'])
                ->setTo($email)
                ->setSubject($this->subject)
                ->send();
        }
        return false;
    }


    /**
     * @param bool $last_id
     * @return array
     */
    public function checkMessage($last_id = false, $tourId = null){
        $result = $this->getAllUsers($tourId);
        return array_filter($result, function($arr) { return $arr['cnt_mess'] > 0 ;});
    }

    /**
     * Method to getAllUsers.
     *
     * @throws ExceptionMessages
     * @return array
     */
    public function getAllUsers($tourId = null) {
        $table_name = Messages::tableName();

        $subQuery = (new Query())
            ->select([
                'from_id',
                'cnt' => new Expression('count(id)'),
                'tourId'
            ])
            ->from($table_name)
            ->where([
                'status' => 1,
                'whom_id' => $this->getIdCurrentUser()
            ])
            ->groupBy([
                'from_id', 'tourId'
            ]);

        $query = (new Query())
            ->select([
                'usr.id',
                $this->attributeNameUser => 'usr.' . $this->attributeNameUser,
                'cnt_mess' => 'msg.cnt',
                'msg.tourId'
            ])
            ->from(['usr' => $this->userTableName])
            ->leftJoin(['msg' => $subQuery], 'usr.id = msg.from_id')
            ->where([
                '!=', 'usr.id', $this->getIdCurrentUser()
            ])
            ->orderBy([
                'msg.cnt' => SORT_DESC,
                'usr.' . $this->attributeNameUser => SORT_DESC
            ]);

        if($this->admins && !in_array(\Yii::$app->user->id, $this->getAdminIds())) {
            $query->andWhere([
                'in', 'usr.id', $this->adminIds
            ]);
        }

        return $query->all();
    }


    /**
     * Method to getAllUsersDialog.
     *
     * @throws ExceptionMessages
     * @return array
     */
    public function getAllUsersDialog()
    {
        $table_name = Messages::tableName();


        $subQuery = (new Query())
            ->select([
                'whom_id',
                'cnt' => new Expression('count(id)'),
                'tourId'
            ])
            ->from($table_name)
            ->where([
                'status' => 1,
            ])
            ->where([
                 '!=', 'whom_id', $this->getIdCurrentUser()
            ])
            ->groupBy([
                'whom_id'
            ]);


        $query = (new Query())
            ->select([
                'usr.id',
//                $this->attributeNameUser => 'usr.' . $this->attributeNameUser,
                $this->attributeNameUser => 'pr.username',
                'cnt_mess' => 'msg.cnt'
            ])
            ->from(['usr' => $this->userTableName])
            ->leftJoin(['msg' => $subQuery], 'usr.id = msg.whom_id')
            ->innerJoin(['pr' => 'profile'], 'usr.id = pr.user_id')
            ->where([
                '!=', 'usr.id', $this->getIdCurrentUser()
            ])
            ->where([
                'not', ['msg.cnt' => null],
                []
            ])
            ->orderBy([
                'msg.cnt' => SORT_DESC,
                'usr.' . $this->attributeNameUser => SORT_DESC
            ]);


        if ($this->admins && !in_array(\Yii::$app->user->id, $this->getAdminIds())) {
            $query->andWhere([
                'in', 'usr.id', $this->adminIds
            ]);
        }

        return $query->all();

    }

    public function getUserTourDialog($userId = null, $tourId = null){
        $table_name = Messages::tableName();

        $subQuery = (new Query())
            ->select([
                'whom_id',
                'cnt' => new Expression('count(id)'),
                'tourId'
            ])
            ->from($table_name)
            ->where([
                'status' => 1,
            ])
            ->where([
                '!=', 'whom_id', $this->getIdCurrentUser()
            ])
            ->where([
                'whom_id' => $userId,
                'tourId' => $tourId
            ])
            ->groupBy([
                'whom_id'
            ]);

        $query = (new Query())
            ->select([
                'usr.id',
//                $this->attributeNameUser => 'usr.' . $this->attributeNameUser,
                $this->attributeNameUser => 'pr.username',
                'cnt_mess' => 'msg.cnt',
                'msg.tourId',
                'theme_topic' => 'tr.name'
            ])
            ->from(['usr' => $this->userTableName])
            ->leftJoin(['msg' => $subQuery], 'usr.id = msg.whom_id')
            ->innerJoin(['pr' => 'profile'], 'usr.id = pr.user_id')
            ->leftJoin(['tr' => 'tour'], 'msg.tourId = tr.id')
            ->where([
                '!=', 'usr.id', $this->getIdCurrentUser()
            ])
            ->where([
//                'not', ['msg.cnt' => null]
                ['is not', 'msg.cnt', null],
            ])
            ->where([
                'tourId' => $tourId
            ])
            ->orderBy([
                'msg.cnt' => SORT_DESC,
                'usr.' . $this->attributeNameUser => SORT_DESC
            ]);

        if ($this->admins && !in_array(\Yii::$app->user->id, $this->getAdminIds())) {
            $query->andWhere([
                'in', 'usr.id', $this->adminIds
            ]);
        }

        return $query->all();

    }

    /**
     * Method to getMessages.
     *
     * @param $whom_id
     * @param $from_id
     * @param $last_id
     * @param $tourId
     * @param $type
     *
     * @throws ExceptionMessages
     * @return array
     */
    protected function getMessages($whom_id, $from_id = null, $type = null, $last_id = null, $tourId = null) {
        $table_name = Messages::tableName();
        $my_id = $this->getIdCurrentUser();

        $query = (new Query())
            ->select([
                'msg.created_at',
                'msg.id',
                'msg.status',
                'msg.message',
                'from_id'   => 'usr1.id',
                'src'       => 'prf1.src_avatar',
//                'from_name' => "usr1.{$this->attributeNameUser}",
                'from_name' => "usr1.username",
                'whom_id'   => 'usr2.id',
                'whom_name' => "usr2.{$this->attributeNameUser}"
            ])
            ->from(['msg' => $table_name])
            ->leftJoin(['usr1' => $this->userTableName], 'usr1.id = msg.from_id')
            ->leftJoin(['prf1' => 'profile'], 'prf1.user_id = msg.from_id')
            ->leftJoin(['usr2' => $this->userTableName], 'usr2.id = msg.whom_id');


        if($from_id) {
            $query
                ->where(['msg.whom_id' => $whom_id, 'msg.from_id' => $from_id])
                ->orWhere(['msg.from_id' => $whom_id, 'msg.whom_id' => $from_id]);
        } else {
            $query->where(['msg.whom_id' => $whom_id]);
        }


        //if not set type
        //send all message where no delete
        if($type) {
            $query->andWhere(['=', 'msg.status', $type]);
        } else {
            /*
            $query->andWhere('((msg.is_delete_from != 1 AND from_id = :my_id) OR (msg.is_delete_whom != 1 AND whom_id = :my_id) ) ', [
                ':my_id' => $my_id,
            ]);
            */
        }

        $query->andWhere('((msg.is_delete_from != 1 AND from_id = :my_id) OR (msg.is_delete_whom != 1 AND whom_id = :my_id) ) ', [
            ':my_id' => $my_id,
        ]);

        if($last_id){
            $query->andWhere(['>', 'msg.id', $last_id]);
        }

        if($tourId){
            $query->andWhere(['msg.tourId' => $tourId]);
        }

        $return = $query->orderBy('msg.id')->all();
        $ids = [];
        foreach($return as $m) {
            if($m['whom_id'] == $my_id) {
                $ids[] = $m['id'];
            }
        }

        //change status to is_read
        if(count($ids) > 0) {
            Messages::updateAll(['status' => Messages::STATUS_READ], ['in', 'id', $ids]);
        }

        $user_id = $this->getIdCurrentUser();
        return array_map(function ($r) use ($user_id) {
            $r['i_am_sender'] = $r['from_id'] == $user_id;
            $r['created_at'] = \DateTime::createFromFormat('U', $r['created_at'])->format('d.m.Y H-i-s');
            return $r;
        },
            $return
        );
    }

    /**
     * Method to getMyMessages.
     *
     * @throws ExceptionMessages
     * @return array
     */
    public function getMyMessages() {
        $id = $this->getIdCurrentUser();
        return $this->getMessages($id);
    }

    /**
     * Method to getAllMessages.
     *
     * @param $whom_id
     * @param $from_id
     * @param $tourId
     * @throws ExceptionMessages
     * @return array
     */
    public function getAllMessages($whom_id, $from_id, $tourId = null) {
        return $this->getMessages($whom_id, $from_id, null, null, $tourId);
    }


    /**
     * @param $whom_id
     * @param $from_id
     * @param $tourId
     * @return array
     */
    public function getNewMessages($whom_id, $from_id, $tourId=null) {
//        debug($tourId);
        return $this->getMessages($whom_id, $from_id, 1, null, $tourId);
    }

    /**
     * Method to getNewMessages.
     *
     * @param $whom_id
     * @param $from_id
     *
     * @throws ExceptionMessages
     * @return array
     */
    protected function _getNewMessages($whom_id, $from_id) {
        return $this->getMessages($whom_id, $from_id, Messages::STATUS_NEW);
    }

    public function saveFile(){
        $upload = new MsgFiles(['scenario' => 'saveFile']);
        if(Yii::$app->request->isAjax){
            $upload->image = UploadedFile::getInstanceByName('file');
            if(!$src = $upload->saveFilesForMessage()){
                /* ToDo add Exception */
            }
            $msgFiles = new MsgFiles();
            $msgFiles->src = $src;

            if(!$msgFiles->save()){
                /* ToDo add Exception */
            }
            return '/uploads/msgfiles/'.$src;
        }
        return false;
    }

    public function deleteFile($src){
        return MsgFiles::deleteFile($src);
    }

}