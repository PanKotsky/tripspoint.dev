<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "messages".
 *
 * @property integer $id
 * @property integer $from_id
 * @property integer $whom_id
 * @property string $message
 * @property integer $status
 * @property integer $is_delete_from
 * @property integer $is_delete_whom
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $tourId
 *
 * @property User $from
 * @property User $whom
 */
class Messages extends \yii\db\ActiveRecord
{
    const STATUS_NEW    = '1';
    const STATUS_READ   = '2';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_id', 'whom_id', 'status', 'is_delete_from', 'is_delete_whom', 'created_at', 'updated_at', 'tourId'], 'integer'],
            [['whom_id', 'message', 'created_at', 'updated_at', 'tourId'], 'required'],
            [['message'], 'string', 'max' => 750],
            [['from_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['from_id' => 'id']],
            [['whom_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['whom_id' => 'id']],
            [['status'], 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => [self::STATUS_NEW, self::STATUS_READ], 'message' => 'Incorrect status'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_id' => 'From ID',
            'whom_id' => 'Whom ID',
            'message' => 'Message',
            'status' => 'Status',
            'is_delete_from' => 'Is Delete From',
            'is_delete_whom' => 'Is Delete Whom',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'tourId' => 'Tour ID'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWhom()
    {
        return $this->hasOne(User::className(), ['id' => 'whom_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        $currentUser = Yii::$app->user->identity->getId();
        if ($currentUser == $this->whom_id) {
            return $this->hasOne(Profile::className(), ['user_id' => 'from_id']);
        } elseif ($currentUser == $this->from_id) {
            return $this->hasOne(Profile::className(), ['user_id' => 'whom_id']);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountAll()
    {
        return $this->find()->count();
    }
}
