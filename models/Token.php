<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 15.09.2017
 * Time: 17:52
 */

namespace app\models;

use dektrium\user\models\Token as BaseToken;
use Yii;

class Token extends BaseToken
{
    /** @inheritdoc */
    public function beforeSave($insert)
    {
        if ($insert) {
            static::deleteAll(['user_id' => $this->user_id]);
            $this->setAttribute('created_at', time());
            $this->setAttribute('code', Yii::$app->security->generateRandomString());
        }

        return parent::beforeSave($insert);
    }

}