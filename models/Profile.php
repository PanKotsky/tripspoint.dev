<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 27.04.2017
 * Time: 12:54
 */

namespace app\models;

use dektrium\user\models\Profile as BaseProfile;
use yii\helpers\BaseHtml;
use yii\helpers\Url;
use yii\web\UploadedFile;
use Yii;
use app\models\User;

/*
 * This is the model class for table "profile".
 *
 * @property integer $user_id
 * @property string  $name
 * @property string  $public_email
 * @property string  $gravatar_email
 * @property string  $gravatar_id
 * @property string  $location
 * @property string  $website
 * @property string  $bio
 * @property string  $timezone
 * @property User    $user
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com
 */

class Profile extends BaseProfile
{
    public $residence;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // firstname rules
            'firstnameRequired' => [['firstname'], 'required'],
            'firstnameTrim'     => ['firstname', 'filter', 'filter' => 'trim'],
            'firstnameLength'   => ['firstname', 'string', 'max'=> 50, 'message' => 'Sorry, but this first name is too long. Maximum length of 50 letters'],
//            'firstnamePattern'  => ['firstname', 'match', 'pattern' => '/[\w-]+/', 'message' => 'Sorry, but firstname can\'t contain numbers'],

             //lastname rules
            'lastnameRequired'  => [['lastname'], 'required'],
            'lastnameTrim'      => ['lastname', 'filter', 'filter' => 'trim'],
            'lastnameLength'    => ['lastname', 'string', 'max'=> 75, 'message' => 'Sorry, but this last name is too long. Maximum length of 75 letters'],
//            'lastnamePattern'   => ['lastname', 'match', 'pattern' => '/[\w-]+/', 'message' => 'Sorry, but lastname can\'t contain numbers'],

            //username rules
            /* ToDo  add scenario for edit */
            'usernameRequired'  => ['username', 'required', 'on'=>'edit'],
            'usernameTrim'      => ['username', 'filter', 'filter' => 'trim'],
            'usernameLength'    => ['username', 'string', 'min' => 3, 'max'=> 25, 'message' => 'Sorry, but this nickname is too short ot too long. Nickname must contain 3-25 symbols'],
            'usernamePattern'   => ['username', 'match', 'pattern' => '/^[-a-zA-Z0-9_\.@]+$/'],
            'usernameUnique'    => ['username', 'unique'],

            //email rules
            'emailRequired'     => ['public_email', 'required'],
            'emailTrim'         => ['public_email', 'filter', 'filter' => 'trim'],
            'emailPattern'      => ['public_email', 'email'],
            'emailUnique'       => ['public_email', 'unique'],

            //phone rules
            /* ToDo  add scenario for edit */
            'phoneRequired'     => ['phone', 'required', 'on'=>'edit'],
            'phoneTrim'         => ['phone', 'filter', 'filter' => 'trim'],
            'phonePattern'      => ['phone', 'match', 'pattern' => '/^[+\-\(\)0-9]+$/'],

            //country rules
            'countryLength'     => ['country', 'string', 'max' => 255],
            'countryTrim'       => ['country', 'filter', 'filter' => 'trim'],
//            'countryPattern'    => ['country', 'match', 'pattern' => '/^[\.-\sA-z]+$/'],

            //region rules
            'regionLength'      => ['region', 'string', 'max' => 255],
            'regionTrim'        => ['region', 'filter', 'filter' => 'trim'],
//            'regionPattern'     => ['region', 'match', 'pattern' => '/^[\.-+\sA-z]+$/'],

            //city rules
            'cityLength'        => ['city', 'string', 'max' => 255],
            'cityTrim'          => ['city', 'filter', 'filter' => 'trim'],
//            'cityPattern'       => ['city', 'match', 'pattern' => '/^[\.-+\'\sA-z]+$/'],

            //address rules
            'addressLength'        => ['address', 'string', 'max' => 255],
            'addressTrim'          => ['address', 'filter', 'filter' => 'trim'],
//            'addressPattern'       => ['address', 'match', 'pattern' => '/^[\.\,\'-+\sA-z0-9]+$/'],

            //zipcode rules
            'zipcodeLength'     => ['zipcode', 'string', 'max' => 20],
            'zipcodeTrim'       => ['zipcode', 'filter', 'filter' => 'trim'],
            'zipcodePattern'    => ['zipcode', 'match', 'pattern' => '/^[-\sA-z0-9]+$/'],

            //avatar rules
            'avatarPattern'     => [['src_avatar'], 'file', 'extensions' => 'gif, jpg, png', 'maxSize' => 1024*1024*0.5],




//            'firstname'            => ['firstname', 'string'],
//            'username'             => ['username', 'string'],
            'timeZoneValidation'   => ['timezone', 'validateTimeZone'],
//            'publicEmailPattern'   => ['public_email', 'email'],
//            'nameLength'           => ['name', 'string', 'max' => 255],
//            'publicEmailLength'    => ['public_email', 'string', 'max' => 255],
            'locationLength'       => ['location', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'            => \Yii::t('user', 'Name'),
            'firstname'       => \Yii::t('user', 'Firstname'),
            'lastname'        => \Yii::t('user', 'Lastname'),
            'username'        => \Yii::t('user', 'Nickname'),
            'public_email'    => \Yii::t('user', 'Email'),
            'gravatar_email'  => \Yii::t('user', 'Gravatar email'),
            'location'        => \Yii::t('user', 'Location'),
            'timezone'        => \Yii::t('user', 'Time zone')
        ];
    }

    /**
     * static method saveNewProfile - save or update userdata in table Profile (for registration new user)
     * @param $userdata - data of user: firstname, lastname, username, public_email
     * @throws \Exception
     * @author Pan_Kockyj
     */
    public static function saveNewProfile($userdata){
        if($userdata) {
            $profile = Profile::findOne(['user_id' => $userdata->id]);
            $profile->firstname = $userdata->firstname;
            $profile->lastname = $userdata->lastname;
            $profile->username = $userdata->username;
            $profile->public_email = $userdata->email;

            $profile->update();
        }
    }

    /**
     * method getAvatar - return user's avatar (if user doesn't have avatar - return default avatar)
     */
    public function getAvatar() {
        if (!empty($this->src_avatar)) {
            return Yii::$app->request->baseUrl.'/uploads/usr/'.$this->user_id .'/'.$this->src_avatar;
        } else {
            return Yii::$app->request->baseUrl.'/uploads/usr/default.png';
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoritesCount()
    {
        return $this->hasMany(Favorites::className(), ['userId' => 'user_id'])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookingsCount()
    {
        return $this->hasMany(Orders::className(), ['userId' => 'user_id'])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewsCount()
    {
        return $this->hasMany(Reviews::className(), ['userId' => 'user_id'])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotosCount()
    {
        return $this->hasMany(TourPhotos::className(), ['userId' => 'user_id'])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewMessagesCount()
    {
        return $this->hasMany(Messages::className(), ['whom_id' => 'user_id'])
            ->andWhere(['status' => 1])
            ->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewOrdersCount()
    {
        return $this->hasMany(Orders::className(), ['providerId' => 'user_id'])
            ->andWhere(['viewedStatus' => 0])
            ->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewBookingsCount()
    {
        return $this->hasMany(Orders::className(), ['userId' => 'user_id'])
            ->andWhere(['viewedStatus' => 1])
            ->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllNotificationsCount()
    {
        return $this->newMessagesCount + $this->newOrdersCount + $this->newBookingsCount;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignment()
    {
        return $this->hasOne(AuthAssignment::className(), ['user_id' => 'user_id']);
    }

    public function getItem_Name(){
        return $this->authAssignment->item_name;
    }
}