<?php

namespace app\controllers;

use app\components\LayoutParamsTrait;
use app\components\Email;
use app\models\Cities;
use app\models\Countries;
use app\models\Groups;
use app\models\Orders;
use app\models\PickupPoints;
use app\models\Reviews;
use app\models\TourLocation;
use app\models\TourPhotos;
use app\models\TourDates;
use app\models\GuideLanguages;
use app\models\Tour;
use app\models\TourSearch;
use app\models\TourTypes;
use Yii;
use yii\web\Controller;
use yii\web\Cookie;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

/**
 * TourController implements the CRUD actions for Tour model.
 */
class TourController extends Controller
{
    use LayoutParamsTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['admin', 'destination', 'supplier'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tour models.
     * @return mixed
     */
    public function actionIndex()
    {
        $tourSearch = new TourSearch();
        $dataProvider = $tourSearch->search(Yii::$app->request->queryParams);

        $typesList = TourTypes::getTypesList(true);

        $continentsList = Countries::getContinentsList();

        return $this->render('index', [
            'tourSearch' => $tourSearch,
            'dataProvider' => $dataProvider,
            'typesList' => $typesList,
            'continentsList' => $continentsList,
        ]);
    }

    /**
     * Get locations list based on the parent location value (for dependent dropdown) for filtering tours
     */
    public function actionChildLocation()
    {
        $parentLocation = Yii::$app->request->post('depdrop_all_params');
        $childLocation = [];
        $outputData = [];
        if (!empty($parentLocation)) {
            if (!empty($parentLocation['toursearch-continent'])) {
                $childLocation = Countries::find()
                    ->joinWith('tourLocations', false, 'RIGHT JOIN')
                    ->where(['countries.continent' => $parentLocation['toursearch-continent']])
                    ->all();
            } elseif (!empty($parentLocation['toursearch-country'])) {
                $childLocation = Cities::find()
                    ->joinWith('tourLocations', false, 'RIGHT JOIN')
                    ->where(['cities.countryId' => $parentLocation['toursearch-country']])
                    ->all();
            }
            if (!empty($childLocation)) {
                foreach ($childLocation as $key => $location) {
                    // Define selected values for the initializing dependent dropdowns after the page was reloaded
                    if ($parentLocation['countryValue'] == $location->id || $parentLocation['cityValue'] == $location->id) {
                        $selected = ['id' => $location->id, 'name' => $location->name];
                    }
                    $outputData[] = ['id' => $location->id, 'name' => $location->name];
                }
                echo json_encode(['output' => $outputData, 'selected' => $selected]);
            }
        } else {
            echo json_encode(['output' => '', 'selected' => '']);
        }
    }

    /**
     * Get dates for the exact guide (for datepicker)
     */
    public function actionGetGuideDates()
    {
        $postData = Yii::$app->request->post();
        if (!empty($postData['tourId']) && !empty($postData['languageId'])) {
            $tour = $this->findModel($postData['tourId']);
            $guideDates = $tour->getGuideDates($postData['languageId']);
            echo $guideDates;
        } else {
            echo json_encode('');
        }
    }

    /**
     * Displays a single Tour model and form for Order creation
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $tour = Tour::findOne(['id' => $id, 'status' => 1]);

        if (!empty($tour)) {
            $guidesArray = [];
            foreach ($tour->guides as $guide) {
                array_push($guidesArray, $guide->language);
            }
            $tourGuides = implode(', ', $guidesArray);

            $order = new Orders(['scenario' => 'scenario_formBookNow']);

            $pickupPoints = PickupPoints::find()
                ->where(['tourId' => $id])
                ->asArray()
                ->all();

            // add guide language to PickupPoints array
            foreach ($pickupPoints as $key => $value) {
                $pickupPoints[$key]['language'] = GuideLanguages::findOne([$value['languageId']])->language;
            }

            $tourReviews = new Reviews();
            $dataProvider = $tourReviews->search($id, Yii::$app->request->queryParams);

            // Add id of the recently viewed tour to the cookies
            if (Yii::$app->request->cookies->has('tourIds')) {
                $tourIdsString = Yii::$app->request->cookies->getValue('tourIds');
                if (strpos($tourIdsString, $id) === false) {
                    $tourIdsArray = explode(',', $tourIdsString);
                    if (count($tourIdsArray) > 50) {
                        array_shift($tourIdsArray);
                        $tourIdsString = implode(',', $tourIdsArray);
                    }
                    Yii::$app->response->cookies->add(new Cookie([
                        'name' => 'tourIds',
                        'value' => $tourIdsString.','.$id,
                    ]));
                }
            } else {
                Yii::$app->response->cookies->add(new Cookie([
                    'name' => 'tourIds',
                    'value' => $id,
                ]));
            }

            return $this->render('view', [
                'tour' => $tour,
                'tourGuides' => $tourGuides,
                'order' => $order,
                'tourPickupPoints' => json_encode($pickupPoints),
                'dataProvider' => $dataProvider,
            ]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * Displays 50 recently viewed Tours
     * @return mixed
     */
    public function actionRecentlyViewed()
    {
        // Get tours ids from the cookies
        if (Yii::$app->request->cookies->has('tourIds')) {
            $error = false;
            $tourIds = explode(',', Yii::$app->request->cookies['tourIds']->value);
            $tours = Tour::findAll(['id' => $tourIds, 'status' => 1]);
        } else {
            $error = true;
            $tours = false;
        }

        return $this->render('recentlyViewed', [
            'error' => $error,
            'tours' => $tours,
        ]);
    }

    /**
     * Creates a new Tour model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $tour = new Tour();
        $newCountry = new Countries();
        $newCity = new Cities();
        $tourLocation = new TourLocation();
        $tourPhotos = new TourPhotos();
        $tourDates = new TourDates();
        $groups = new Groups();
        $pickupPoints = new PickupPoints();

        $languagesList = GuideLanguages::getLanguagesList();

        $typesList = TourTypes::getTypesList();

        if ($tour->load(Yii::$app->request->post()) &&
            $newCountry->load(Yii::$app->request->post()) &&
            $newCity->load(Yii::$app->request->post()) &&
            $tourDates->load(Yii::$app->request->post()) &&
            $tourPhotos->load(Yii::$app->request->post()) &&
            $pickupPoints->load(Yii::$app->request->post()) &&
            $groups->load(Yii::$app->request->post())) {

            $country = Countries::findOne(['name' => $newCountry->name]);
            if (!$country) {
                $newCountry->setContinent();
                $newCountry->save();
                $country = $newCountry;
            }
            $city = Cities::findOne(['name' => $newCity->name, 'countryId' => $newCountry->id]);
            if (!$city) {
                $newCity->countryId = $newCountry->id;
                $newCity->save();
                $city = $newCity;
            }

            $tour->providerId = Yii::$app->user->identity->getId();
            $tour->deposit = (Yii::$app->params['depositPercentage'] / 100) * $tour->priceAdult;
            $tour->descExtra = json_encode($tour->descExtra);
            $tour->inclusion = json_encode($tour->inclusion);
            $tour->exclusion = json_encode($tour->exclusion);
            $itinerary = [];
            foreach ($tour->itinerary as $key => $values) {
                $itinerary[$values['itineraryTime']] = $values['itineraryDesc'];
            }
            $tour->itinerary = json_encode($itinerary);
            $tour->duration = Tour::wordsToSeconds($tour->duration);
            $tour->payMethod = json_encode($tour->payMethod);

            if ($tour->save()) {
                $tourLocation->tourId = $tour->id;
                $tourLocation->countryId = $country->id;
                $tourLocation->cityId = $city->id;
                $tourLocation->save();

                foreach ($tourDates->allDates as $key => $values) {
                    $tourDates = new TourDates();
                    $tourDates->tourId = $tour->id;
                    $tourDates->attributes = $values;
                    $tourDates->save();
                }
                
                $tourPhotos->tourId = $tour->id;
                $tourPhotos->userId = Yii::$app->user->identity->getId();
                $promoImage = $tourPhotos->uploadPromoImage($tourPhotos->promoImage);
                if ($tourPhotos->save()) {
                    $path = $tourPhotos->getImageFile($tour->id);
                    file_put_contents($path, $promoImage);
                }

                $images = UploadedFile::getInstances($tourPhotos, 'images');
                foreach ($images as $image) {
                    $tourPhoto = new TourPhotos();
                    $tourPhoto->tourId = $tour->id;
                    $tourPhoto->userId = Yii::$app->user->identity->getId();
                    $imageResource = $tourPhoto->uploadImage($image);
                    if ($tourPhoto->save()) {
                        $path = $tourPhoto->getImageFile($tour->id);
                        imagejpeg($imageResource, $path);
                    }
                }

                $allPoints = json_decode($pickupPoints->allPoints, true);
                foreach ($allPoints as $point) {
                    $pickupPoint = new PickupPoints();
                    $pickupPoint->tourId = $tour->id;
                    $pickupPoint->attributes = $point;
                    $pickupPoint->save();
                }

                if (!empty($groups->newGroups)) {
                    foreach ($groups->newGroups as $key => $values) {
                        $group = new Groups();
                        $group->tourId = $tour->id;
                        $group->attributes = $values;
                        $group->save();
                    }
                }

                // Send email to supplier (template name, mailTo, params for template)
                Email::send('addedTour', $tour->provider->emailProvider, ['model' => $tour]);

                return $this->redirect(['/supplier/added-tours']);
            }
        } else {
            return $this->render('create', [
                'tour' => $tour,
                'country' => $newCountry,
                'city' => $newCity,
                'tourPhotos' => $tourPhotos,
                'tourDates' => $tourDates,
                'languagesList' => $languagesList,
                'typesList' => $typesList,
                'groups' => $groups,
                'pickupPoints' => $pickupPoints,
            ]);
        }
    }

    /**
     * !!! NEEDS REFACTORING AND REWRITING CODE !!!
     * Updates an existing Tour model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the user hasn't rights
     */
    public function actionUpdate($id)
    {
        $tour = $this->findModel($id);
        if (!empty(Yii::$app->user->identity) && Yii::$app->user->identity->getId() == $tour->providerId) {
            $tour->duration = $tour->secondsToValuesArray($tour->duration);
            $tour->descExtra = json_decode($tour->descExtra);
            $tour->inclusion = json_decode($tour->inclusion);
            $tour->exclusion = json_decode($tour->exclusion);
            $itinerary = [];
            foreach (json_decode($tour->itinerary) as $key => $value) {
                $itinerary[] = [
                    'itineraryTime' => $key,
                    'itineraryDesc' => $value,
                ];
            }
            $tour->itinerary = $itinerary;
            $tour->payMethod = json_decode($tour->payMethod);

            $tourLocation = $tour->tourLocations;
            $country = $tour->countries;
            $city = $tour->cities;

            $tourPhotos = new TourPhotos();
            $existingPhotos = [];
            foreach ($tour->allPhotos as $photo) {
                $existingPhotos[] = Yii::$app->request->baseUrl.'/uploads/photos/'.$photo->tourId.'/'.$photo->src;
            }

            $tourDates = new TourDates();
            $tourDates->allDates = $tour->tourDates;

            $groups = new Groups();
            $groups->newGroups = $tour->groups;

            $pickupPoints = new PickupPoints();
            $allPoints = [];
            foreach ($tour->pickupPoints as $pickupPoint) {
                $allPoints[] = $pickupPoint->attributes;
            }
            $pickupPoints->allPoints = json_encode($allPoints);

            $typesList = TourTypes::getTypesList();
            $languagesList = GuideLanguages::getLanguagesList();

            if ($tour->load(Yii::$app->request->post()) &&
                $country->load(Yii::$app->request->post()) &&
                $city->load(Yii::$app->request->post()) &&
                $tourDates->load(Yii::$app->request->post()) &&
                $tourPhotos->load(Yii::$app->request->post()) &&
                $pickupPoints->load(Yii::$app->request->post()) &&
                $groups->load(Yii::$app->request->post())) {

                $existingCountry = Countries::findOne(['name' => $country->name]);
                if (!$existingCountry) {
                    $country->setContinent();
                    $country->save();
                }

                $exisitngCity = Cities::findOne(['name' => $city->name, 'countryId' => $city->id]);
                if (!$exisitngCity) {
                    $city->countryId = $country->id;
                    $city->save();
                }

                $tour->providerId = Yii::$app->user->identity->getId();
                $tour->deposit = (Yii::$app->params['depositPercentage'] / 100) * $tour->priceAdult;
                $tour->descExtra = json_encode($tour->descExtra);
                $tour->inclusion = json_encode($tour->inclusion);
                $tour->exclusion = json_encode($tour->exclusion);
                $itinerary = [];
                foreach ($tour->itinerary as $key => $values) {
                    $itinerary[$values['itineraryTime']] = $values['itineraryDesc'];
                }
                $tour->itinerary = json_encode($itinerary);
                $tour->duration = Tour::wordsToSeconds($tour->duration);
                $tour->payMethod = json_encode($tour->payMethod);
                $tour->status = 0;

                if ($tour->save()) {
                    $tourLocation->tourId = $tour->id;
                    $tourLocation->countryId = $country->id;
                    $tourLocation->cityId = $city->id;
                    $tourLocation->save();

                    foreach ($tour->tourDates as $tourDate) {
                        $tourDate->delete();
                    }
                    foreach ($tourDates->allDates as $key => $values) {
                        $tourDates->tourId = $tour->id;
                        $tourDates->attributes = $values;
                        $tourDates->save();
                    }

                    if (!empty($tourPhotos->promoImage)) {
                        $tour->promoPhoto->delete();
                        $tourPhotos->tourId = $tour->id;
                        $tourPhotos->userId = Yii::$app->user->identity->getId();
                        $promoImage = $tourPhotos->uploadPromoImage($tourPhotos->promoImage);
                        if ($tourPhotos->save()) {
                            $path = $tourPhotos->getImageFile($tour->id);
                            file_put_contents($path, $promoImage);
                        }
                    }

                    if (!empty($tourPhotos->images)) {
                        $images = UploadedFile::getInstances($tourPhotos, 'images');
                        foreach ($images as $image) {
                            $tourPhoto = new TourPhotos();
                            $tourPhoto->tourId = $tour->id;
                            $tourPhoto->userId = Yii::$app->user->identity->getId();
                            $imageResource = $tourPhoto->uploadImage($image);
                            if ($tourPhoto->save()) {
                                $path = $tourPhoto->getImageFile($tour->id);
                                imagejpeg($imageResource, $path);
                            }
                        }
                    }

                    foreach ($tour->pickupPoints as $pickupPoint) {
                        $pickupPoint->delete();
                    }
                    $allPoints = json_decode($pickupPoints->allPoints, true);
                    foreach ($allPoints as $point) {
                        $pickupPoint = new PickupPoints();
                        $pickupPoint->tourId = $tour->id;
                        $pickupPoint->attributes = $point;
                        $pickupPoint->save();
                    }

                    if (!empty($tour->groups)) {
                        foreach ($tour->groups as $group) {
                            $group->delete();
                        }
                    }

                    if (!empty($tour->groups)) {
                        foreach ($groups->newGroups as $key => $values) {
                            $group = new Groups();
                            $group->tourId = $tour->id;
                            $group->attributes = $values;
                            $group->save();
                        }
                    }

                    return $this->redirect(['/supplier/added-tours']);
                }
            } else {
                return $this->render('update', [
                    'tour' => $tour,
                    'country' => $country,
                    'city' => $city,
                    'tourPhotos' => $tourPhotos,
                    'existingPhotos' => $existingPhotos,
                    'tourDates' => $tourDates,
                    'groups' => $groups,
                    'pickupPoints' => $pickupPoints,
                    'typesList' => $typesList,
                    'languagesList' => $languagesList,
                ]);
            }
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * Deletes an existing Tour model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tour model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tour the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tour::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException();
        }
    }
}
