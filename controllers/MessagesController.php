<?php
/**
 * Created by PhpStorm.
 * User: PanKockyj
 * Date: 31.08.2017
 * Time: 16:20
 */

namespace app\controllers;

use app\components\LayoutParamsTrait;
use yii\web\Controller;
use Yii;
use dektrium\user\Finder;
use yii\web\NotFoundHttpException;

class MessagesController extends Controller {
    use LayoutParamsTrait;
    //use UserProfile;

    /** @var Finder */
    protected $finder;

    public $layout;

    /**
     * @param string           $id
     * @param \yii\base\Module $module
     * @param Finder           $finder
     * @param array            $config
     */
    public function __construct($id, $module, Finder $finder, $config = [])
    {
        $this->finder = $finder;
        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'private-messages' => [
                'class' => \app\actions\MessageApiAction::className()
            ]
        ];
    }

    /**
     * Display preview of all dialogs current user or open dialogue
     * @return string
     */

    public function actionDialogs(){
        /*
         * переглянути код у звязку з заміною трейта
         *
        $userId = Yii::$app->user->identity->getId();
        $isSupplier = false;
//        $avatar = Profile::getDefaultAvatar($userId);
        $user = $this->getUser($userId);

        if(Yii::$app->user->can('updateProfile')) {
            $isSupplier = true;
        }

        $this->view->params['userdata'] = $user[0];
        $this->view->params['avatar'] = $user[1];
        $this->view->params['isSupplier'] = $isSupplier;
        */
        if(!empty(Yii::$app->request->get())) {
            $userId = Yii::$app->request->get('ui');
            $tourId = Yii::$app->request->get('ti');

            /*необхідно додати перевірку на кількість повідомлень і роль юзера
             * якщо повідомлень 0, а поточний користувач не є саплаєром, необхідно показати відсутність діалогу
             *  і відсутню можливість відправки повідомлення
             * */

            return $this->render('dialogs', [
                'userId' => $userId,
                'tourId' => $tourId
            ]);
        }
        else{
            return $this->render('msg');
        }
    }

}