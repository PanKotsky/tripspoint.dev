$(document).ready(function () {
    $('.l-comments__list').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: false,
        autoplaySpeed: 5000,
        arrows: false,
        draggable: false,
        dotsClass: "l-comments__dotsList",
        rows: 2
    });
});