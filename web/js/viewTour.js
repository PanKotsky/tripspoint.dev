// array pickupPoints was passed from php and contains all tour's pickup points
var markers = pickupPoints;
function initMap() {
    var map = new google.maps.Map(document.getElementById("map"), {maxZoom: 16});
    var infowindow = new InfoBox({
        closeBoxURL: ''
    });
    var marker, i;
    var bounds = new google.maps.LatLngBounds();

    // Loop through array of markers and place each one on the map
    for (i = 0; i < markers.length; i++) {
        var markerPosition = new google.maps.LatLng(markers[i]['lat'], markers[i]['lng']);
        bounds.extend(markerPosition);
        marker = new google.maps.Marker({
            position: markerPosition,
            icon: '/img/map/map_marker.png',
            map: map
        });

        // Allow each marker to have an info window
        marker.addListener('click', (function (marker, i) {
            return function () {
                infowindow.setContent(
                    '<div class="c-pickupPoints__form">' +
                        '<div class="c-pickupPoints__title">' +
                            markers[i]['name'] +
                        '</div>' +
                        '<div class="c-pickupPoints__guide">' +
                            'Guide: ' +
                            '<span class="c-pickupPoints__guide--name">' + markers[i]['language'] + '</span>' +
                        '</div>' +
                        '<div class="c-pickupPoints__time">' +
                            'Departure at: ' +
                            '<span class="c-pickupPoints__time--clock">' + markers[i]['time'] + '</span>' +
                        '</div>' +
                        '<div class="c-pickupPoints__desc">' +
                            markers[i]['addInfo'] +
                        '</div>' +
                    '</div>'
                );
                infowindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }
}

// function to sum all travellers
var numberCount = $('.c-bookingForm__quantity');
function findTotal(){
    var total = 0;
    numberCount.each(function(){
        total += +$(this).val();
    });
    $('.c-bookingForm__selectedText').text(total + ' traveller(s)');
}

// stick the right block ("book now")
// --- TEMPORARY SOLUTION ---
Array.prototype.slice.call(document.querySelectorAll('.aside1')).forEach(function(a) {
    var b = null, P = 0;
    window.addEventListener('scroll', Ascroll, false);
    document.body.addEventListener('scroll', Ascroll, false);
    function Ascroll() {
        if (b == null) {
            var Sa = getComputedStyle(a, ''), s = '';
            for (var i = 0; i < Sa.length; i++) {
                if (Sa[i].indexOf('overflow') == 0) {
                    s += Sa[i] + ': ' +Sa.getPropertyValue(Sa[i]) + '; '
                }
            }
            b = document.createElement('div');
            b.style.cssText = s + ' width: ' + a.offsetWidth + 'px;';
            a.insertBefore(b, a.firstChild);
            var l = a.childNodes.length;
            for (var i = 1; i < l; i++) {
                b.appendChild(a.childNodes[1]);
            }
        }
        var Ra = a.getBoundingClientRect(),
            R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('.article1').getBoundingClientRect().bottom + 0);
        if ((Ra.top - P) <= 0) {
            if ((Ra.top - P) <= R) {
                b.className = 'stop';
                b.style.top = - R +'px';
                b.style.left = 0;
            } else {
                b.className = 'sticky';
                b.style.top = P + 'px';
                b.style.left = Ra.left + 'px';
            }
        } else {
            b.className = '';
            b.style.top = '';
            b.style.left = '';
        }
    }
});

$(document).ready(function() {
    // init Google Maps
    initMap();
    
    // init jQuery UI Tabs and enable/disable all inputs from next/previous tabs
    $("#c-bookingForm__tabs").tabs({
        activate: function( event, ui ) {
            var oldTabId = ui.oldPanel[0].id;
            var newTabId = ui.newPanel[0].id;
            if (oldTabId == 'tabs-travellers') {
                $("#"+oldTabId+" :enabled").prop('disabled', true);
                $("#"+newTabId+" :disabled").prop('disabled', false);
            } else if (oldTabId == 'tabs-groups') {
                $("#"+oldTabId+" :enabled").prop('disabled', true);
                $("#"+newTabId+" :disabled").prop('disabled', false);
            }
        }
    });

    // toggle list
    $('.js-bookingForm__selection').click(function () {
        $('.js-bookingForm__wrapper').css("display", "block");
        $('.c-bookingForm__modalWindow').toggleClass("hide");
        $('.c-bookingForm__selectedIcon').toggleClass("c-bookingForm__selectedIcon--rotated");
        findTotal();
    });

    // show guide and date block on group select click
    $('.field-orders-groupid').click(function() {
        $('.js-bookingForm__wrapper').css("display", "block");
    });

    // Increase, decrease travellers
    $('.c-bookingForm__quantity--plus').click(function(){
        var fieldName = $(this).attr('field');
        var currentVal = parseInt($("input[name='"+fieldName+"']").val());
        $("input[name='"+fieldName+"']").val(currentVal + 1);
        findTotal();
    });
    $(".c-bookingForm__quantity--minus").click(function() {
        var fieldName = $(this).attr('field');
        var currentVal = parseInt($("input[name='"+fieldName+"']").val());
        if (currentVal > 1) {
            $("input[name='"+fieldName+"']").val(currentVal - 1);
        } else {
            if(fieldName != 'Orders[totalAdult]'){
                $("input[name='"+fieldName+"']").val(0);
            }
        }
        findTotal();
    });

    $('#chosenGuide').change(function() {
        $.ajax({
            type: 'POST',
            url: '/tour/get-guide-dates',
            data: {
                tourId: $('#orders-tourid').val(),
                languageId: this.value
            }
        }).done(function(data) {
            // enable div for click on datepicker
            $('.c-bookingForm__tool').removeClass('c-bookingForm__tool--disabled');
            // var guideDates was created earlier for datepicker init
            guideDates = $.parseJSON(data);
            // reset datepicker
            $('#guideDatepicker').kvDatepicker('update', '');
        });
    });

    // slow scroll to anchor (menu item)
    var root = $('html, body');
    $('.c-tabNav__point').click(function() {
        root.animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top - 50
        }, 500);
        return false;
    });

    // ReadMore/Less Toggle
    $(".js-readMore").click(function() {
        $(".js-textComplete").toggleClass("completeText");
        $(this).text(function (i, text) {
            return text === 'Read more ›' ? 'Read less ›' : 'Read more ›'
        });
    });

    // init sticky-kit for menu
    $(".l-newLocation__navWrapper").stick_in_parent({
        parent: '.l-newLocation'
    });

    // add/remove to/from wishlist
    $("#wishListLink").click(function(e) {
        e.preventDefault();
        if ($(this).data('userId')) {
            $.post('/favorites/create', {userId: $(this).data('userId'), tourId: $(this).data('tourId')})
                .done(function(data){
                    if (data == 'created') {
                        $('#wishListLink').addClass('is-active').find('span').text('Remove from wishlist');
                    } else if (data == 'deleted') {
                        $('#wishListLink').removeClass('is-active').find('span').text('Add to wishlist');
                    }
                })
                .fail(function(){
                    alert('Error');
                });
        } else {
            alert('You must be logged in!');
        }
    });

    // init jQuery lightGallery plugin by click on button
    $('.js-photoGallery').lightGallery();
    $('.js-showPhotoGallery').click(function() {
        $(this).parent().find('.js-photoGallery :first-child').trigger('click');
    });
});