var markers = [];
var pickupPoints = $.parseJSON($("#pickuppoints-allpoints").val());
var uniqueId = 1;
var infos = [];

function initMap() {
    var map = new google.maps.Map(document.getElementById("map"), {});
    var bounds = new google.maps.LatLngBounds();

    var languagesList = $.parseJSON($("#languagesList-json").val());

    // Loop through array of markers and place each one on the map
    for (var i = 0; i < pickupPoints.length; i++) {
        var markerPosition = new google.maps.LatLng(pickupPoints[i]['lat'], pickupPoints[i]['lng']);
        bounds.extend(markerPosition);
        var marker = new google.maps.Marker({
            position: markerPosition,
            icon: '/img/map/map_marker.png',
            map: map
        });

        marker.id = pickupPoints[i]['id'];
        //Add marker to the array.
        markers.push(marker);

        var pointLanguages = '';
        $.each(languagesList, function(id, text){
            if (id == pickupPoints[i]['languageId']) {
                pointLanguages += '<option class="c-mapForm__option" value="'+ id +'" selected>'+ text +'</option>';
            } else {
                pointLanguages += '<option class="c-mapForm__option" value="'+ id +'">'+ text +'</option>';
            }
        });

        // Allow each marker to have an info window
        marker.addListener('click', (function (marker, i) {
            return function () {
                var infowindow = new InfoBox({
                    closeBoxURL: '',
                    content:
                        '<div class="c-mapForm">' +
                        '<div class="c-mapForm__title">' +
                        'Point' +
                        '</div>' +
                        '<div class="c-mapForm__block">' +
                        '<input type="hidden" id="pointLat" value=' + pickupPoints[i]['lat'] + '>' +
                        '<input type="hidden" id="pointLng" value=' + pickupPoints[i]['lng'] + '>' +
                        '<div class="c-mapForm__label">' +
                        'Name the point' +
                        '</div>' +
                        '<input class="c-mapForm__input" id="pointName" placeholder="- Point name -" value=' + pickupPoints[i]['name'] + '>' +
                        '<div class="c-mapForm__label">' +
                        'Tour guide' +
                        '</div>' +
                        '<div class="c-mapForm__selectWrapper">' +
                        '<select class="c-mapForm__select" id="pointLanguage">' +
                        '<option class="c-mapForm__option" disabled selected>- Select guide -</option>' +
                            pointLanguages +
                        '</select>' +
                        '</div>' +
                        '<div class="c-mapForm__label">' +
                        'Departure time' +
                        '</div>' +
                        '<input type="time" class="c-mapForm__input" id="pointTime" placeholder="- 24-hour format, e.g. 21:00 -" value=' + pickupPoints[i]['time'] + '>' +
                        '<div class="c-mapForm__label">' +
                        'Additional information' +
                        '</div>' +
                        '<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information">' + pickupPoints[i]['addInfo'] + '</textarea>' +
                        '<div class="c-mapForm__buttonGroup">' +
                        '<button type="button" class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarker(' + pickupPoints[i]['id'] + ')">Delete</button>' +
                        '<button type="button" class="c-button c-button--noneTransform" onclick="saveData()">Save the point</button>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                });
                //close the previous infowindow
                closeInfos();
                infowindow.open(map, marker);
                //keep the handle, in order to close it on next click event
                infos[0] = infowindow;
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    map.addListener('click', function(event) {
        var location = event.latLng;

        var marker = new google.maps.Marker({
            position: location,
            icon: '/img/map/map_marker.png',
            map: map
        });

        //Set unique id
        marker.id = uniqueId;
        uniqueId++;

        var pointLanguages = '';
        $.each(languagesList, function(id, text){
            pointLanguages += '<option class="c-mapForm__option" value="'+ id +'">'+ text +'</option>';
        });

        var infowindow = new InfoBox({
            closeBoxURL: '',
            content:
                '<div class="c-mapForm">' +
                '<div class="c-mapForm__title">' +
                'Point' +
                '</div>' +
                '<div class="c-mapForm__block">' +
                '<input type="hidden" id="pointLat" value=' + marker.getPosition().lat() + '>' +
                '<input type="hidden" id="pointLng" value=' + marker.getPosition().lng() + '>' +
                '<div class="c-mapForm__label">' +
                'Name the point' +
                '</div>' +
                '<input class="c-mapForm__input" id="pointName" placeholder="- Point name -">' +
                '<div class="c-mapForm__label">' +
                'Tour guide' +
                '</div>' +
                '<div class="c-mapForm__selectWrapper">' +
                '<select class="c-mapForm__select" id="pointLanguage">' +
                '<option class="c-mapForm__option" disabled selected>- Select guide -</option>' +
                    pointLanguages +
                '</select>' +
                '</div>' +
                '<div class="c-mapForm__label">' +
                'Departure time' +
                '</div>' +
                '<input type="time" class="c-mapForm__input" id="pointTime" placeholder="- 24-hour format, e.g. 21:00 -">' +
                '<div class="c-mapForm__label">' +
                'Additional information' +
                '</div>' +
                '<textarea class="c-mapForm__input c-mapForm__textarea" id="pointAddInfo" placeholder="Additional information"></textarea>' +
                '<div class="c-mapForm__buttonGroup">' +
                '<button class="c-button c-button--transparentButton c-button--noneTransform" onclick="deleteMarker(' + marker.id + ')">Delete</button>' +
                '<button class="c-button c-button--noneTransform" onclick="saveData()">Save the point</button>' +
                '</div>' +
                '</div>' +
                '</div>'
        });

        marker.addListener('click', function() {
            //close the previous infowindow
            closeInfos();
            infowindow.open(map, marker);
            //keep the handle, in order to close it on next click event
            infos[0] = infowindow;
        });

        //Add marker to the array.
        markers.push(marker);
    });
}

function saveData() {
    var point = {};
    point.name = document.getElementById('pointName').value;
    point.languageId = document.getElementById('pointLanguage').value;
    point.time = document.getElementById('pointTime').value;
    point.lat = document.getElementById('pointLat').value;
    point.lng = document.getElementById('pointLng').value;
    point.addInfo = document.getElementById('pointAddInfo').value;

    //Remove similar pickupPoint's data from array
    for (var i = 0; i < pickupPoints.length; i++) {
        if (pickupPoints[i].lat == point.lat && pickupPoints[i].lng == point.lng) {
            pickupPoints.splice(i, 1);
        }
    }

    pickupPoints.push(point);

    //Insert all PickupPoints into input on the page
    $("#pickuppoints-allpoints").val(JSON.stringify(pickupPoints));

    closeInfos();
}

function deleteMarker(id) {
    //Find and remove the marker from the Array
    for (var i = 0; i < markers.length; i++) {
        if (markers[i].id == id) {
            //Remove the marker from Map
            markers[i].setMap(null);
            //Remove the marker and data from arrays
            markers.splice(i, 1);
            pickupPoints.splice(i, 1);
        }
    }
    
    //Insert all PickupPoints into input on the page
    $("#pickuppoints-allpoints").val(JSON.stringify(pickupPoints));
}

function closeInfos(){
    if(infos.length > 0){
        //detach the infowindow from the marker
        infos[0].set("marker", null);
        //and close it
        infos[0].close();
        //blank the array
        infos.length = 0;
    }
}

$(document).ready(function() {
    // init Google Maps
    initMap();

    // init jQuery steps
    $(".l-addTour").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "fade",
        enableFinishButton: false,
        labels: {
            previous: "< Previous step",
            next: "Next step"
        },

        onInit: function (event, currentIndex) {
            $('.actions > ul > li:first-child').attr('style', 'display:none');
        },

        onStepChanged: function (event, currentIndex, priorIndex) {
            if (currentIndex > 0) {
                $('.actions > ul > li:first-child').attr('style', '');
            } else {
                $('.actions > ul > li:first-child').attr('style', 'display:none');
            }

            if (currentIndex == 5) {
                $('.actions > ul').attr('style', 'display:none');
            }

            // reload map
            if (currentIndex == 4) {
                initMap();
            }
            return true;
        }
    });


    var autocomplete = new google.maps.places.Autocomplete(
        (document.getElementById('locationAutocomplete')),
        {types: ['(cities)']});

    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        place.address_components.forEach(function(component) {
            component.types.forEach(function(type) {
                if (type === 'country') {
                    $("#countries-id").val(component.short_name);
                    $("#countries-name").val(component.long_name);
                } else if (type === 'locality') {
                    $("#cities-name").val(component.long_name);
                }
            });
        });
    });

    var cropPreview = $('#cropPreview');
    var cropResult = $('#cropResult');
    var fileInput = $('#fileInput');
    var cropButton = $('#cropButton');
    var imageInfo = $('#imageInfo');
    var imageTitle = $('#imageTitle');
    var deleteImage = $('#deleteImage');
    var promoImage = $('#tourphotos-promoimage');

    var uploadCrop = cropPreview.croppie({
        // inner borders
        viewport: {
            width: 1024,
            height: 400
        }
    });

    fileInput.on('change', function (e) {
        var reader = new FileReader();
        reader.onload = function (e) {
            uploadCrop.croppie('bind', {
                url: e.target.result
            });
        };
        reader.readAsDataURL(this.files[0]);

        imageTitle.text(e.target.files[0].name);

        fileInput.prop('disabled', true);
        imageInfo.show();
        cropButton.show();
    });

    cropButton.on('click', function () {
        uploadCrop.croppie('result', {
            size: 'viewport',
            format: 'jpeg'
        }).then(function (resp) {
            cropPreview.parent().hide();
            cropButton.hide();

            promoImage.val(resp);

            var html = '<img src="' + resp + '" />';
            cropResult.html(html);
        });
    });

    deleteImage.on("click", function() {
        cropResult.empty();
        cropPreview.parent().show();

        //reset croppie plugin
        uploadCrop.croppie('bind', {
            url: ''
        });

        fileInput.val('');
        fileInput.prop('disabled', false);
        imageInfo.hide();
        cropButton.hide();
    });

    $('#removePoints').click(function() {
        //Remove all markers from the map
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        //Clear array of markers
        markers = [];
        //Clear all pickupPoints data and array of it
        $("#pickuppoints-allpoints").val('');
        pickupPoints = [];
    });

    // enable input with price per person on checkbox checked
    $('.c-tourForm__checkboxInput').on('change', function() {
        $(this).parent().next().find(':input').prop('disabled', !this.checked);
    });
});