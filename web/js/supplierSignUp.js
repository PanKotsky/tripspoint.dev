/**
 * Created by Java-PC on 09.08.2017.
 */
$(document).ready(function () {
    var input = $(".js-profileForm__input");
    var countInputs = input.length;
    for (var i = 0; i <= countInputs; i++) {
        if (input.eq(i).val() == "") {
            input.eq(i).next(".js-profileForm__label").addClass("is-active");
        }
    }

    input.click(function () {
        $(this).next(".js-profileForm__label").removeClass("is-active");
    });

    input.focusout(function() {
        if($(this).val().trim() == '') {
            $(this).next(".js-profileForm__label").addClass("is-active");
        }
    });

    /* JUI TABS*/
    $( "#c-tabs__supplier").tabs();


    /* File input change */
    var fileInput = $('#file');
    var linkInfo = $('#linkInfo');
    var linkTitle = $('#linkTitle');
    var deleteLink = $('#deleteLink');

    fileInput.on('change', function (e) {
        var i = 1;
        var fileObject = e.target.files;
        if ( i < fileObject.length) {
            linkTitle.text('Files: ' + fileObject.length);
        } else {
            linkTitle.text(e.target.files[0].name);
        }

        // fileInput.prop('disabled', true);
        linkInfo.show();
    });

    deleteLink.on("click", function() {
        fileInput.val('');
        // fileInput.prop('disabled', false);
        linkInfo.hide();
    });
});

var placeSearch, autocomplete;
var componentForm = {
    street_number: 'long_name',
    route: 'long_name',
    locality: 'long_name',
    country: 'long_name',
    postal_code: 'short_name'
};

function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        {types: ['geocode']});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

    for (var component in componentForm) {
        console.log(component);
        if(component != 'street_number') {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            // console.log(place.address_components);

            /* Extended functional for merging name street and number of house */
            if (addressType == 'route') {
                var val = place.address_components[i][componentForm[addressType]] + ', ' + place.address_components[i-1]['long_name'];
                document.getElementById(addressType).value = val;
            }
            else if(addressType == 'country'){
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
                var codeCountry = place.address_components[i].short_name;
                document.getElementById('countryId').value = codeCountry;
                // console.log(codeCountry);
            }
            else if(addressType == 'street_number'){
                continue;
            }
            else {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }
}





